'use strict';

angular.module('exidelife.web')
  .controller('BannerCtrl', function ($scope, $state, $http, $q, utilService,  MenuService) {

        $scope.openSearch = function (){
            //alert ("index.html#/search/" + $scope.srch+"/"+$scope.search_context )    ;
//            $window.location.href = "index.html#/search/" + $scope.srch+"/"+$scope.search_context ;
            $state.go('main.search',{'srch':$scope.srch,'context':$scope.search_context});
        }

        $scope.select_search = function (id, name){
            $scope.search_context = id;
            $scope.search_menu_item = "in " + name;
        }


        function init() {

            var cat_exclusion = utilService.get_cat_exclusion();

            //$scope.loading = true;
            $scope.search_menu_item = "in All Categories"     ;
            $scope.search_context = 0;
            MenuService.get_level1_menu($http, $q).then(function (data) {

                    var level1_menu=[];

                    if (cat_exclusion.length > 0) {

                        for (var i = 0; i < data.length; i++) {
                            var level1_cat = data[i];
                            var cat_excluded = false;
                            if (cat_exclusion.length > 0) {
                                for (var j = 0; j < cat_exclusion.length; j++) {
                                    if (level1_cat.name.toLowerCase() === cat_exclusion[j].toLowerCase()) {
                                        cat_excluded = true;
                                        break;
                                    }
                                }
                            }
                            if (cat_excluded === false) {
                                level1_menu.push(level1_cat);
                            }
                        }

                        $scope.level1_menu_items = level1_menu;
                    }
                    else {
                        $scope.level1_menu_items = data;
                    }
                },
                function(){


                    //Display an error message
                    $scope.error= error;
                });

        }
        var selected_address = {};
        init();
  });
