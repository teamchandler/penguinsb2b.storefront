'use strict';

angular.module('exidelife.web')
  .controller('ProdCtrl', function ($scope,$state, $stateParams,  $timeout, $rootScope, $http, $q, $location,
                                    productService, catService, cartService,
                                     utilService, localStorageService, NotificationService) {

        $scope.show_loader=false;
        var product_id =  $stateParams.prodid;
        $scope.product_id = product_id;




        $scope.order_quantity="";

        var threshold_quantity=utilService.get_threshold_value();

        //localStorageService.set('first_product_index', "0");

        var ProdList = [];
        ProdList = utilService.nextList;

        if (typeof ProdList === 'undefined') {
            $scope.MultipleItems = 0;
        }
        else {
            $scope.AllProductList = ProdList;
            $scope.MultipleItems = ProdList.length;
            var Prod_Id_Url = "";

            for (var i = 0; i < ProdList.length; i++) {
                if (product_id == ProdList[i].id) {
                    //if (localStorageService.get('first_product_index') != "0") {
                    //    localStorageService.set('first_product_index', i.toString());
                    //}
                    if ((i + 1) == ProdList.length) {
                        Prod_Id_Url = ProdList[0].id;
                    }
                    else {
                        Prod_Id_Url = ProdList[i + 1].id;
                    }
                    $scope.prod_id = Prod_Id_Url;
                }
            }

            //var PodFrstIndex = localStorageService.get('first_product_index') * 1;
            //alert(PodFrstIndex);
            //PodFrstIndex = PodFrstIndex.toFixed(0);

            //for (m = PodFrstIndex; m < (ProdList.length + PodFrstIndex) ; m++) {
            //    var j = m > ProdList.length ? (m - ProdList.length) : m;
            //    Prod_Id_Url = ProdList[j].id;
            //    $scope.prod_id = Prod_Id_Url;
            //}

        }


        var prod_view = {
            user_id: "",
            id: "",
            sku: "",
            brand: "",
            Name: "",
            price: "",
            Express: ""
        }
        init();
        function check_login() {
            if (localStorageService.get('user_info') == null) {
                $location.path("/login");
            }
        }
        function init() {
            check_login();
            var product_details, parent_product, child_product;
            child_product = [];
            productService.get_productDetailsById($http, $q, product_id).then(function (data) {
                    product_details = data;
                    //console.log(product_details[0]);
                    //console.log(JSON.parse(product_details[0]).price);
                    parent_product = product_details[0];


                   // get_prod_price_list(JSON.parse(product_details[0].price));
                    get_prod_shipping_days(JSON.parse(parent_product).parent_cat_id);
                    $scope.selected_image = JSON.parse(parent_product).image_urls[0].link;
                    $scope.selected_image_zoom = JSON.parse(parent_product).image_urls[0].zoom_link;
                    if (product_details.length > 1) {
                        $scope.size_ind = true;

                        for (i = 1; i < product_details.length; i++) {

                            var child_prod_block = 0;
                            var child_prod_detail=[];
                            var child_prod_stock=[];
                            child_prod_detail=JSON.parse(product_details[i]);
                            child_prod_block =  child_prod_detail.block_number;

                            child_prod_stock=JSON.parse(JSON.parse(product_details[i]).stock);
                            if(child_prod_stock.length>0){
                                var total_stock = 0;
                               // var stock_after_block = 0;
                                angular.forEach(child_prod_stock, function(item) {
                                    total_stock += item.stock;
                                 //   if(child_prod_block) {

                                   //     total_stock = (total_stock - child_prod_block);
                                   // }
                                   // else
                                  //  {
                                  //      total_stock = total_stock;

                                  //  }
                                });

                                if(child_prod_block) {

                                        total_stock = (total_stock - child_prod_block);
                                     }

                                if (total_stock > 0) {
                                    $scope.out_of_stock = 0;

                                    if(total_stock<threshold_quantity){
                                        $scope.stock_msg = "* Only "+total_stock+" left";
                                        child_prod_detail.stock_msg= "* Only "+total_stock+" left";
                                    }
                                    else{
                                        $scope.stock_msg = "In stock";
                                        child_prod_detail.stock_msg= "In stock";
                                    }
                                    child_prod_detail.current_stock=total_stock;

                                }
                                else {
                                    $scope.out_of_stock = 1;
                                    $scope.stock_msg = "Out of stock";
                                    child_prod_detail.current_stock=total_stock;
                                    child_prod_detail.stock_msg="Out of stock";
                                }

                               // child_prod_detail.in_stock_qty=total_stock;
                            }

                            child_prod_detail.stock=child_prod_stock;
                            child_prod_detail.ord_qty="";

                            child_product.push(child_prod_detail);

                        }
                        console.log (child_product);
                        if (child_product.length > 0) {
                            $scope.out_of_stock = 0;
                            $scope.stock_msg = "In stock";
                        }
                        else {
                            $scope.out_of_stock = 1;
                            $scope.stock_msg = "Out of stock";
                        }

                    }
                    else {
                        $scope.size_ind = false;
                        var stock = JSON.parse(parent_product).stock;
                        //console.log(stock);
                        if (stock <= 0) {
                            $scope.out_of_stock = 1;
                            $scope.stock_msg = "Out of stock";
                        }
                        else {
                            if ((stock <= 10) && (stock > 0)) {
                                $scope.out_of_stock = 0;
                                $scope.stock_msg = "Only " + stock.toString() + " left";
                            }
                            else {
                                $scope.out_of_stock = 0;
                                $scope.stock_msg = "In stock";
                            }
                        }
                    }
                    $scope.sizes = [];

                    console.log($scope.sizes);
                    //console.log($scope.out_of_stock);
                    $scope.child_product=child_product;
//                    for(var j=0;j<child_product.length;j++ )
//                    {
//                        $scope.size = child_product[j].size;
//                        prod_sizes.push($scope.size);
//
//
//                    }
                    process_price_logic(JSON.parse(parent_product));

                    $scope.brand_url = "#/brand/" + JSON.parse(parent_product).brand.toLowerCase();
                    $scope.brand_logo = s3_brand_logo_url + JSON.parse(parent_product).brand.toLowerCase() + ".jpg";
                    // getting breadcrumb at end intentionally so that it doesn't block others
                    get_bread_crumb(JSON.parse(parent_product).parent_cat_id);
                },
                function () {
                    //Display an error message
                    $scope.error = "Product Data not found";
                });
            $scope.$watch('myModel', function (v) {
                //console.log('changed', v);
                $scope.selected_prod = v;
                //alert (v);
            });

        }



        $scope.calorderqty = function () {

            // todo check with stock (summation of multiple w/h) so that user can't place order with qty > stock
            var prod_ord_qty=[];
            prod_ord_qty= $scope.child_product;
            var total_ord_qty = 0;
            angular.forEach(prod_ord_qty, function(item) {
                if(item.ord_qty!="" && item.ord_qty!=null)
                {
                    if(item.ord_qty <= item.current_stock){
                        total_ord_qty += parseInt(item.ord_qty);
                    }
                    else
                    {
                        item.ord_qty="";
                        alert('This number of quantity is not available');
                    }

                }
            });
            $scope.order_quantity=total_ord_qty;

            var prod_price_list=$scope.product_price;
            if(prod_price_list.length>0) {
                var prod_price=0;
                if(total_ord_qty>0){
                    angular.forEach(prod_price_list, function(item) {
                        if(total_ord_qty==item.min_qty || total_ord_qty==item.max_qty || (total_ord_qty>item.min_qty && total_ord_qty<item.max_qty))
                        {
//                            prod_price = parseInt(item.list);
                              prod_price = item.list.toFixed(0);
                        }
                    });
                    $scope.order_amount=prod_price*total_ord_qty;
                }
                else
                {
                    $scope.order_amount="";
                }
            }

        }





        function get_prod_price_list(prodpricelist){

            var priceqtylist=[];

//            for (i = 0; i < prodpricelist.length; i++) {
//                prod_price = JSON.parse(prodpricelist[i].toString());
//
//                if ((typeof prod.price) == "object") {
//                    prod_price = prod.price;
//                }
//                else {
//                    prod_price = JSON.parse(prod.price.toString());
//                }
//
//                if (excluded === false) {
//                    priceqtylist.push(prod);
//                }
//            }

        }

        var process_price_logic = function (prod) {
            var productList = [];
            var conv_ratio = utilService.get_conversion_ratio();
            var store_type = utilService.get_store_type();
            var customer_discount = utilService.get_customer_discount();
            var cat_discount = utilService.get_cat_discount();
            var sku_discount = utilService.get_sku_discount();

            var brand_discount = utilService.get_brand_discount();

            $scope.store_type = store_type;
            var arr_cat, arr_sku, special_price;
            $scope.productDetails = prod;
            $scope.productDetails.mrp = prod.price[0].mrp;

            var prod_price_array=prod.price;
            var array_prod_price=[];
            angular.forEach(prod_price_array, function(item) {
                if(item.list!="" && item.list!=null)
                {
                    if(item.max_qty<=2000){
                        item.final_offer=item.list.toFixed(0);
                        array_prod_price.push(item);
                    }

                }
            });

            //prod.product_price = JSON.parse($scope.productDetails.price);
//            $scope.product_price = prod.price;
              $scope.product_price = array_prod_price;


        }


        var get_bread_crumb = function (cat_id) {
            if (cat_id != null) {
                catService.get_breadcrumb($http, $q, cat_id).then(function (data) {
                        //Update UI using data or use the data to call another service
                        var cats = data;
                        $scope.cats = [];
                        for (i = 0; i < cats.length; i++) {
                            $scope.cats.push(JSON.parse(cats[i].toString()));
                        }
                        for (i = 0; i < $scope.cats.length; i++) {
                            var id = $scope.cats[i].id;
                            var tmp = id.split(".");
                            var level;
                            if (tmp.length <= 2) {
                                // level 1
                                level = 1
                            }
                            else {
                                if (tmp[2] == "0") {
                                    level = 2;
                                }
                                else {
                                    level = 3;
                                }
                            }
                            $scope.cats[i].level = level;
                        }
                    },
                    function () {
                        //Display an error message
                        $scope.error = error;
                    });
            }
        }

        $scope.addTocart = function (productDetails) {
            $scope.show_loader=true;

            var cart_record={};
            cart_record.parent_prod_detail=$scope.productDetails;
            //cart_record.parent_prod_detail.description  = "";
            cart_record.child_prod_detail=productDetails;
            cart_record.order_quantity=$scope.order_quantity;
            cart_record.order_amount=$scope.order_amount;
            console.log(cart_record);
                $timeout(function () {
                    cartService.addTocart(cart_record,$http, $q);
                    $scope.show_loader=false;
                    NotificationService.cart_added("cart added", cart_record)
                }, 1000);

           // $scope.show_loader=false;

        }

        $scope.changeImage = function (img, $element) {
            $scope.selected_image = img.zoom_link;
            $scope.selected_image_zoom = img.zoom_link;
        }

        $scope.addtowishlist = function (productDetails) {
            //cartService.addTocart(productDetails.id, 1, productDetails.price.mrp, productDetails.price.mrp);

            if (($scope.myOptions != "") && ($scope.selected_prod == "")) {
                // this is a condition when we have sizes but it's not selected
                alert("Please select size !");
            }
            else {
                $timeout(function () {
                    cartService.addTowishlist(productDetails, $scope.selected_prod, $scope.myOptions);
                    $rootScope.$broadcast('wishlist_changed');
                }, 1000);

            }

        }

        $scope.TestSizeQty = function () {
            alert('HI');
            var aaa=$scope.ordersizeqty;
            alert(aaa[0].quantity);

        }


        $scope.nextProduct = function (productDetails) {
            var next_list = utilService.get_next_list();
            var next_item;
            for (var i=0; i < next_list.length; i++ ){
                if (next_list[i].sku == productDetails.sku){
                    if (i < next_list.length){
                        next_item = i+1;
                    }
                }
            }
            utilService.change_state($state, "main.prod",{"prodid": next_list[next_item].sku});
        }

        $scope.prevProduct = function (productDetails) {
            var next_list = utilService.get_next_list();
            var next_item;
            for (var i=0; i < next_list.length; i++ ){
                if (next_list[i].sku == productDetails.sku){
                    if (i > 0){
                        next_item = i-1;
                    }
                }
            }
            utilService.change_state($state, "main.prod",{"prodid": next_list[next_item].sku});
        }

        var get_prod_shipping_days = function (cat_id) {


            var ship;
            var ship1;
            var ship2;

            productService.get_prod_ship($http, $q, cat_id).then(function (data) {
                    //Update UI using data or use the data to call another service
                    ship = data[0];
                    ship1 = JSON.parse(ship).Shipping;
                    ship2 = ship1.split(" ");
                    $scope.shipping = ship2[0];
                },
                function () {
                    //Display an error message
                    $scope.error = error;
                });
        }





        $scope.go_to_enquire = function()
        {
            $state.go('main.enq',{'product_id':product_id});

        }


        angular.module('exidelife.web').config(function($sceProvider) {

            $sceProvider.enabled(false);
        });



    });
