/**
 * Created by DRIPL_1 on 08/06/2014.
 */


angular.module('exidelife.web')
    .controller('myenquiryCtrl', function ($scope, $http, $q,
                                           $location, my_enquiryService,
                                           localStorageService, utilService) {



        init();

        function init() {

            get_enquire_data();
            get_general_enquire_data();
        }

        function get_enquire_data() {
            my_enquiryService.get_enquire_data($http, $q, localStorageService.get('user_info').email_id).then(function (data) {
                    $scope.my_enquiry = data;
                    var enquire_list = [];
                    angular.forEach(data, function(item) {
                        if(item.size_quantity!="" && item.size_quantity!=null)
                        {

                            var size_qty = JSON.parse(item.size_quantity);
                            angular.forEach(size_qty, function(item1) {

                                var oe={};
                                oe.enquire_id=item.id;
                                oe.size=item1.size;
                                oe.ord_qty = item1.ord_qty;
                                oe.id = item1.id;
                                oe.name = item1.name;
                                oe.enquire_status = item.enquire_status;


                                enquire_list.push(oe);
                            });


                        }
                    });


                    $scope.enquiry = enquire_list;

                    //console.log($scope.monthly_tracker)
                },
                function () {
                    //Display an error message
                    $scope.error = error;
                    $scope.loading = false;
                });
        }


        function get_general_enquire_data() {
            my_enquiryService.get_general_enquire_data($http, $q, localStorageService.get('user_info').email_id).then(function (data) {
                    $scope.general_enquire = data;


                    //console.log($scope.monthly_tracker)
                },
                function () {
                    //Display an error message
                    $scope.error = error;
                    $scope.loading = false;
                });
        }










    });
