/**
 * Created by DRIPL_1 on 06/10/2014.
 */

'use strict';

angular.module('exidelife.web')
    .controller('shipCtrl', function ($scope,shipService,$state,$window,localStorageService,utilService,$http,$q,$location,cartService) {
        console.log("home");
         var x ;
        function check_login (){
            if (localStorageService.get('user_info') == null){
                $window.location.href = root_url + "/index.html#/login";
            }
            else
            {
                $window.location.href = root_url + "/home.html";
            }
        }

//        $scope.$on('cal amount', function() {
//
//            $scope.order_amount=($scope.total_order_amount *1)+($scope.shipping_charge *1);
//
//        });

      $scope.user_shipping_list;


        $scope.select_address = function(selected_address){
            populate_selected_address(selected_address);
        }
       var get_user_shipping = function(){
            $scope.loading = true;
            shipService.get_user_shipping_list($http, $q, localStorageService.get('user_info').email_id).then(function(data){

                    $scope.user_shipping_list =  data;
                    //console.log($scope.user_shipping_list) ;
                    $scope.loading = false;
                },
                function(){
                    //Display an error message
                    $scope.error= error;
                    $scope.loading = false;
                });
        }
        $scope.save_data = function() {


            // $window.location.href = "http://localhost:8000/index.html#/main/payment";
            // $location.path("/payment/769");

//            if (selected_address.name == ""){
//                alert ("Please enter Shipping Address Name")
//            }else{
//                if (selected_address.address == ""){
//                    alert ("Please enter Shipping Address")
//                }
//                else{
//                    if (selected_address.city == ""){
//                        alert ("Please enter Shipping Address City")
//                    }
//                    else {
//                        if (selected_address.pincode == ""){
//                            alert ("Please enter Shipping Address Postal Code")
//                        }
//                        else {
//                            if (selected_address.mobile_number== ""){
//                                alert ("Please enter Mobile Number")
//                            }else{
//            var ship_data = [];
//            ship_data.push(cartService.get_cart());
//            ship_data.push(selected_address);

            var company = utilService.get_store().toLowerCase();

            if (typeof $scope.selected_address === 'undefined' ){
                alert ("One of the required fields is missing. Please make sure that you have provided all required information!");
                return false;
            }

            else if ($scope.selected_address.billing_name == "" || typeof $scope.selected_address.billing_name === 'undefined' ){
                alert ("One of the required fields is missing. Please make sure that you have provided all required information!");
                return false;
            }
            else if ($scope.selected_address.address == "" || typeof $scope.selected_address.address === 'undefined' ){
                alert ("One of the required fields is missing. Please make sure that you have provided all required information!");
                return false;
            }
            else if ($scope.selected_address.locality == "" || typeof $scope.selected_address.locality === 'undefined' ){
                alert ("One of the required fields is missing. Please make sure that you have provided all required information!");
                return false;
            }
            else if ($scope.selected_address.city == "" || typeof $scope.selected_address.city === 'undefined' ){
                alert ("One of the required fields is missing. Please make sure that you have provided all required information!");
                return false;
            }
            else if ($scope.selected_address.company == "" || typeof $scope.selected_address.company === 'undefined' ){
                alert ("One of the required fields is missing. Please make sure that you have provided all required information!");
                return false;
            }
            else if ($scope.selected_address.pin == "" || typeof $scope.selected_address.pin === 'undefined' ){
                alert ("One of the required fields is missing. Please make sure that you have provided all required information!");
                return false;
            }
            else if ($scope.selected_address.state == "" || typeof $scope.selected_address.state === 'undefined' ){
                alert ("One of the required fields is missing. Please make sure that you have provided all required information!");
                return false;
            }
            else if ($scope.selected_address.mobile_no == "" || typeof $scope.selected_address.mobile_no === 'undefined' ){
                alert ("One of the required fields is missing. Please make sure that you have provided all required information!");
                return false;
            }
           else if(company == 'puma') {

                if ($scope.selected_address.sales_person_name == "" || typeof $scope.selected_address.sales_person_name === 'undefined') {
                    alert("One of the required fields is missing. Please make sure that you have provided all required information!");
                    return false;
                }
                else if ($scope.selected_address.sales_person_mobile == "" || typeof $scope.selected_address.sales_person_mobile === 'undefined') {
                    alert("One of the required fields is missing. Please make sure that you have provided all required information!");
                    return false;
                }
            }
            selected_address.user_id=localStorageService.get('user_info').email_id;
            selected_address.store=utilService.get_store();
            selected_address.shipping_amount=$scope.shipping_charge;
            selected_address.cart_total=$scope.total_cart_amount;
            selected_address.address_populated = $scope.user_shipping_list;

            $scope.show_loader=true;
            shipService.save_shipping_page($http, $q, selected_address).then(function (data) {

                $scope.show_loader=false;
                localStorageService.set('selected_ship_address', selected_address);
                // check for express
                //console.log(cartService.get_Cart())    ;
                var c = cartService.get_cart();
                var express = false;
                for (var i = 0; i < c.length; i++) {
                    if (c[i].express) {
                        express = true;
                        break;
                    }
                }
                if (express) {
                    // have express - send to express_popup
                    if (utilService.get_express_shipping() == 1) {
                        $location.path("/exp/" + data.toString());
                    }
                    else {
                        $location.path("/pp/" + data.toString());
                    }
                }
                else {
                    // no express - send directly to payment page
                    //$location.path("/payment/" + data.toString());
                    //	$window.location.href = root_url + "/index.html#/main/payment/" + data.toString();
                    $state.go('main.payment', {'order_id': data.toString()})
                }
            },
            function () {
                //Display an error message
                $scope.show_loader=false;
                $scope.error = error;
            });



        }
        var populate_selected_address = function (address){
            selected_address.id = address.id;
            selected_address.name = address.name;
            selected_address.shipping_name = address.shipping_name;
            selected_address.address= address.address;
            selected_address.city= address.city;
            selected_address.state = address.state;
            selected_address.pincode = address.pincode;
            selected_address.mobile_number= address.mobile_number;
            selected_address.user_id = localStorageService.get('user_info').email_id;
            //selected_address.store = store_signature;
            selected_address.store = utilService.get_store().toLowerCase();
            $scope.selected_address = selected_address;
        }





        function init() {
            $scope.loading = true;
            // load state and country
             $scope.state_list = shipService.get_state_list();
             $scope.country_list = shipService.get_country_list();
            get_profile_info();   // Atul this function gets user shipping address and calculates shipping charges also.
            get_total_order_amount();
            populate_selected_address({
                id : 0, name : "", shipping_name : "",
                address: "", city: "", state :"",
                pincode : "", mobile_number : "", user_id : localStorageService.get('user_info').email_id,
                store : utilService.get_store().toLowerCase() //store_signature
            });
            $scope.loading = false;

        }

   function get_profile_info(){


            var email_id= localStorageService.get('user_info').email_id;

            shipService.get_profile_info($http, $q, email_id)
                .then
            (
                function (data) {

                    console.log(data);
                   // $scope.user_data=data[0][0];
                    $scope.user_shipping_list = data;

                    state= $scope.user_shipping_list.state;

                  $scope.shipping_charge=  cal_shipping_charge();

                },

                function () {
                    //Display an error message
                    $scope.error = error;
                }
            );

        }

//        $scope.$watch('selected_address.state', function () {
     //       $scope.shipping_charge = cal_shipping_charge() ;
//            localStorageService.set('shipping',$scope.shipping_charge );
//        });

        var state ="";


        var cal_shipping_charge = function () {

            var cart_items = cartService.get_cart();
            var shipping_charge =0;
            console.log(cart_items);
            var total_amount_billable=0;

          /*  angular.forEach(cart_items, function(crt_itm) {

                if(state=="Karnataka"){

                    var shipping_chaarge_item_wise=0;
                    var child_prod_ord_qty=crt_itm.order_quantity;
                    var prod_price_list=crt_itm.parent_prod_detail.price;

                    if ((child_prod_ord_qty >= 10)&&(child_prod_ord_qty <= 100))

                    {
                        shipping_charge= child_prod_ord_qty * 50;
                    }

                    else if ((child_prod_ord_qty >= 101)&&(child_prod_ord_qty <= 300))

                    {
                        shipping_charge= child_prod_ord_qty * 45;
                    }


                    else if ((child_prod_ord_qty > 300))

                    {
                        shipping_charge= child_prod_ord_qty * 35;
                    }

                }
                else{
                    var shipping_chaarge_item_wise=0;
                    var child_prod_ord_qty=crt_itm.order_quantity;
                    var prod_price_list=crt_itm.parent_prod_detail.price;

                    if ((child_prod_ord_qty >= 10)&&(child_prod_ord_qty <= 100))

                    {
                        shipping_charge= child_prod_ord_qty * 100;
                    }

                   else if ((child_prod_ord_qty >= 101)&&(child_prod_ord_qty <= 300))

                    {
                        shipping_charge= child_prod_ord_qty * 90;
                    }


                    else if ((child_prod_ord_qty > 300))

                    {
                        shipping_charge= child_prod_ord_qty * 75;
                    }

                }



            });*/

            return shipping_charge;

        };

        var get_total_order_amount = function () {

            var tmp = cartService.get_cart();
            console.log(tmp);
            var total_amount ;
            var min_shipping = utilService.get_min_shipping();
            var shipping_charge = utilService.get_shipping_charge();
            var shipping = 0;
            var total_amount_billable=0;
            var total_amount_final_ord = 0;
            for (var i = 0; i < tmp.length; i++) {
            //    total_amount_final_ord = total_amount_final_ord + tmp[i].parent_prod_detail.price[0].final_offer * tmp[i].order_quantity;
                total_amount_final_ord = total_amount_final_ord + (tmp[i].order_amount*1);
            }

//            if (total_amount_final_ord < min_shipping) {
//                shipping  = shipping_charge;
//            }
//            else{
//                shipping = 0;
//            }

          //  shipping= $scope.shipping_charge;

            total_amount_billable = total_amount_final_ord + shipping ;
           $scope.total_cart_amount=total_amount_billable.toFixed(0);
        };

        var selected_address = {};
        init();


    });
