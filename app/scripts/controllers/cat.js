'use strict';

angular.module('exidelife.web')
  .controller('CatCtrl', function ( $scope, $state, $stateParams,
                                     $timeout, $location,  $http,$q,
                                     catService, utilService,
                                     cartService, state_managerService,
                                     localStorageService

        ) {
        //I like to have an init() for controllers that need to perform some initialization. Keeps things in
        //one place...not required though especially in the simple example below
        var cat_id = $stateParams.catid;


        $scope.spcl_cat=cat_id;


        console.log($scope.spcl_cat);

        var max_price_sku=0;
        var min_price_sku=0;

        $scope.$on('cal_max_price', function() {
            alert('cal_max_price');
        });
        $scope.$on('cal_min_price', function() {
            alert('cal_min_price');
        });

        init();

        function set_filter(cat_id) {
            // get data
//             alert (cat_id);
            var cat_details, selected_cat;// = catService.get_cat_details(cat_id, $http, $q);
//            catService.get_cat($http, $q, cat_id).then(function (data) {
//                    //Update UI using data or use the data to call another service
//                    cat_details = data;
//                    if (cat_details.length != 0) {
////                        //console.log(data);
//                        selected_cat = JSON.parse(cat_details[0].toString());
//                        $scope.cat_name = selected_cat.Name;
//                        $scope.filters = selected_cat.filters;
//                        //console.log($scope.filters);
//                        $scope.checked_vals = [selected_cat.filters[0]];
//                    }
//                    else {
//                        //selected_cat =  JSON.parse(cat_details[0].toString());
//                        $scope.cat_name = "";
//                        $scope.filters = [];
//                        $scope.checked_vals = [selected_cat.filters[0]];
//                    }
//                },
//                function () {
//                    //Display an error message
//                    $scope.error = error;
//                });

            catService.get_cat_details($http, $q, cat_id).then(function (data) {
                    if (data.length != 0) {
                        if (JSON.parse(data[0].toString()).image_urls.length > 0) {
                            $scope.cat_image = JSON.parse(data[0].toString()).image_urls[0].link;
                        }
                        else {
                            $scope.cat_image = "";
                        }
                    }
                    else {
                        $scope.cat_image = "";
                    }

                },

                function () {
                    //Display an error message
                    $scope.error = error;
                });

            catService.get_breadcrumb($http, $q, cat_id).then(function (data) {
                    //Update UI using data or use the data to call another service
                    var id, level;
                    var cats = data;
                    $scope.cats = [];
                    // hardcoded home node
                    var home = '{"id": "0","Name": "Home","description": "Go to Home", "level": 0}';
                    $scope.cats.push(JSON.parse(home));
                    for ( i = 0; i < cats.length; i++) {
                        $scope.cats.push(JSON.parse(cats[i].toString()));
                    }
                    for (var i = 0; i < $scope.cats.length; i++) {
                        id = $scope.cats[i].id;
                        var tmp = id.split(".");
                        if (tmp.length <= 2) {
                            // level 1
                            level = 1
                        }
                        else {
                            if (tmp[2] == "0") {
                                level = 2;
                            }
                            else {
                                level = 3;
                            }
                        }
                        $scope.cats[i].level = level;
                    }
                    //                console.log($scope.cats);
                },
                function () {
                    //Display an error message
                    $scope.error = error;
                });
            $scope.checked_vals = ""

        }
        $scope.refresh_filters = function () {
            //console.log($scope.checked_vals ) ;
            //$scope.checked_vals = [];
            set_filter(cat_id);

        };


        var user_info;
        var brand_exclusion = utilService.get_brand_exclusion();
        function init() {
            // check user as this is loaded in all pages
            //check_login();
            user_info = localStorageService.get('user_info');
            var lastChar = cat_id[cat_id.length - 1]; // => "1"
            //console.log (cat_id.split(".").length)     ;
            if ((lastChar == "0") && (cat_id.split(".").length == 2)) { // top level menu
                $location.path("/cat1/" + cat_id);
            }
            $scope.cat_id = cat_id;
            $scope.sortorder = 'Name';
            set_filter(cat_id);
            var prod_list;

            catService.get_prod_by_cat($http, $q, cat_id, user_info.min_point_range, user_info.max_point_range).then(function (data) {
                    //Update UI using data or use the data to call another service
                    prod_list = data;
                    //console.log(prod_list);
                    process_price_logic(prod_list);
                    get_brands();
                    get_shipping_days();
                    //get_brands(prod_list);
                    //console.log($scope.brands_filter)  ;
                },

                function () {
                    //Display an error message
                    $scope.error = error;
                });
            // search brands within searched prod list



        }
        var process_price_logic = function (prod_list) {
            var productList = [];
            var conv_ratio = utilService.get_conversion_ratio();
            var store_type = utilService.get_store_type();
            var filter_display = utilService.get_filter_display(); //Added By Hassan Ahamed
            var customer_discount = utilService.get_customer_discount();
            var cat_discount = utilService.get_cat_discount();
            var sku_discount = utilService.get_sku_discount();
            var brand_discount = utilService.get_brand_discount();

            //console.log(cat_discount);
            var arr_cat, arr_sku, special_price;
            special_price = 0;

            $scope.store_type = store_type;
            $scope.filter_display = filter_display; //Added By Hassan Ahamed Start
            for (var i = 0; i < prod_list.length; i++) {
                productList.push(JSON.parse(prod_list[i].toString()));
                //console.log( productList[i].price);
                if (angular.isArray(productList[i].price)){

                    if(productList[i].price.length>0){

                        productList[i].max_price =_.max(productList[i].price,
                            function (p) {
                                return p.final_offer;
                            }).final_offer.toFixed(0);
                        productList[i].min_price_where =_.findWhere(productList[i].price, {"max_qty" : 2000.0});
                        if(angular.isUndefined(productList[i].min_price_where))
                        {
                            productList[i].min_price = 0;
                        }
                        else
                        {
                            productList[i].min_price = _.pick(productList[i].min_price_where, 'final_offer', function (p) {
                                return p.final_offer;
                            }).final_offer.toFixed(0);
                        }


                        // productList[i].min_price =_.min(productList[i].price,
                        //     function (p) {
                        //         return p.final_offer;
                        //    }).final_offer.toFixed(0);
                        productList[i].mrp =productList[i].price[0].mrp.toFixed(0);
                    }
                    else
                    {
                        productList[i].mrp =0;
                        productList[i].min_price = 0 ;
                        productList[i].max_price = 0;
                    }


                }
                else{
                    productList[i].mrp =productList[i].price.mrp.toFixed(0);
                    productList[i].min_price = 0 ;
                    productList[i].max_price = 0;
                }
            }

//            utilService.set_next_list(productList);
//            utilService.nextList = productList;

            $scope.productList = productList;
            //console.log($scope.productList);
            if (filter_display == "P") {//Added By Hassan Ahamed Start

                if (productList.length > 0) {
                    $scope.productList = productList;
                    $scope.productList_original = productList;
                    $scope.special_price = special_price;
                    //console.log(productList);
//                    $scope.max_price_sku = _.max($scope.productList_original,
//                        function (p) {
//                            return parseInt(p.point);
//                        }).point;
//                    $scope.min_price_sku = _.min($scope.productList_original,
//                        function (p) {
//                            return parseInt(p.point);
//                        }).point;

                    $scope.max_price_sku = 0;
                    $scope.min_price_sku = 0;
                }
            }

            else {//Added By Hassan Ahamed End

                if (productList.length > 0) {
                    $scope.productList = productList;
                    $scope.productList_original = productList;
                    $scope.special_price = special_price;

                    $scope.max_price_sku = _.max($scope.productList_original,
                        function (p) {
                            return parseInt(p.price[0].mrp);
                        }).price[0].mrp;

                    $scope.min_price_sku = _.min($scope.productList_original,
                        function (p) {
                            return parseInt(p.price[0].mrp);
                        }).price[0].mrp;

//                    angular.forEach($scope.productList_original, function(item) {
//                        max_price_sku =_.max(item.price,
//                            function (p) {
//                                return parseInt(p.mrp);
//                            });
//                    });
//
//                    angular.forEach($scope.productList_original, function(item) {
//                        min_price_sku =_.min(item.price,
//                            function (p) {
//                                return parseInt(p.mrp);
//                            });
//                    });

                }
            }
        };
        //var get_brands = function (prod_list){
        var get_brands = function () {
            var brands_filter, tmp = [];
            for (var i = 0; i < $scope.productList.length; i++) {
                var excluded = false;
                var prod_brand = $scope.productList[i].brand;
                //console.log(prod_brand.toLowerCase())   ;
                if (brand_exclusion.length > 0) {
                    for (var j = 0; j < brand_exclusion.length; j++) {
                        if (prod_brand.toLowerCase() === brand_exclusion[j].toLowerCase()) {
                            excluded = true;
                            break;
                        }
                    }
                }
                if (excluded === false) {
                    tmp.push(
                        prod_brand.toUpperCase()
                    )
                }

            }
            $scope.brand_filter = _.uniq(tmp);
        };


        var get_shipping_days = function () {
            var tmp = [];
            for (var i = 0; i < $scope.productList.length; i++) {
                var excluded = false;
                var prod_shipping_days = $scope.productList[i].shipping_days;
                if (excluded === false) {
                    tmp.push(prod_shipping_days);
                }
            }
            var shipping_days_array=[];
            var shipping_days=[];
            shipping_days_array = _.uniq(tmp);
            var min_ship_days=[];
            angular.forEach(shipping_days_array, function(item) {
                var tmp = item.split("-");
                min_ship_days.push(parseInt(tmp[0]));
            });

            for(var i=0;i<shipping_days_array.length;i++){
                var min_ship=0;
                min_ship =_.min(min_ship_days,function (p) {return parseInt(p);});
                angular.forEach(shipping_days_array, function(item) {
                    var tmp = item.split("-");
                    if(parseInt(tmp[0])==min_ship){
                        shipping_days.push(item);
                        var index = min_ship_days.indexOf(min_ship);
                        if (index > -1) {
                            min_ship_days.splice(index, 1);
                        }
                    }

                });

            }
            $scope.shipping_days_filter=shipping_days;
            console.log(shipping_days);
            console.log($scope.shipping_days_filter);
        };

        var filters = [];

        $scope.apply_filter = function (filter_name, filter_value) {
            var found = false;
            for (var i = 0; i < filters.length; i++) {
                if (filters[i].name == filter_name) {
                    if (filters[i].value == filter_value) {
                        // special treatment for discount and price filters
                        if ((filter_name != "discount") && (filter_name != "price")) {
                            filters.splice(i, 1);
                            found = true;
                        }
                    }
                }
            }

            if (!found) {
                filters.push({ name: filter_name, value: filter_value });
            }
            if (filter_name == "price") {
                var filter_price = true;
            }
            if (filter_name == "discount") {
                var filter_discount = true;
            }

            var brand_filter = _.where(filters, { name: "brand" });
            var shipping_days_filter = _.where(filters, { name: "shipping_days" });
            var price_filter = _.where(filters, { name: "price" });
            var discount_filter = _.where(filters, { name: "discount" });
            var feature_filter = _.difference(filters, brand_filter,shipping_days_filter, price_filter, discount_filter);// .without(filters, {name:"brand"});


            // apply filter on original productlist and copy to display one
            var prodlist_tmp = $scope.productList_original;

            if (filters.length != 0) {
                var l_found;
                var l_brand_found;
                var l_ship_found;

                var prodList_filtered = [];
                var prodList_brand_filtered = [];
                var prodList_ship_filtered = [];
                var prod_list_feature_filtered = [];
                var prod_list_price_filtered = [];
                var prod_list_discount_filtered = [];

                // now filter for brand
                if (brand_filter.length > 0) {
                    for (var i = 0; i < prodlist_tmp.length; i++) {
                        l_brand_found = false;
                        var prod_tmp = prodlist_tmp[i];
                        for (var p = 0; p < brand_filter.length ; p++) {
                            if (prod_tmp.brand.toLowerCase() == brand_filter[p].value.toLowerCase()) {
                                l_brand_found = true;
                                break;
                            }
                        }
                        if (l_brand_found) {
                            prodList_brand_filtered.push(prod_tmp);
                        }

                    }
                }
                else {
                    prodList_brand_filtered = prodlist_tmp;
                }
                //console.log(prodList_filtered);

                // now filter for Shipping Days apply that on prodList_filtered
                if (shipping_days_filter.length > 0) {
                    for (var i = 0; i < prodList_brand_filtered.length; i++) {
                        l_ship_found = false;
                        var prod_tmp = prodlist_tmp[i];
                        for (var p = 0; p < shipping_days_filter.length ; p++) {
                            if (prod_tmp.shipping_days == shipping_days_filter[p].value) {
                                l_ship_found = true;
                                break;
                            }
                        }
                        if (l_ship_found) {
                            prodList_filtered.push(prod_tmp);
                        }

                    }
                }
                else {
                    prodList_filtered = prodList_brand_filtered;
                }

                // now filter for pricerange apply that on prodList_filtered
                if (filter_price) {

                    for (var i = 0; i < prodList_filtered.length; i++) {

                        var prod_tmp = prodList_filtered[i];

                        //console.log(prod_tmp);

                        if (filter_display == "P") { //Added By Hassan Ahamed Start

//                            if (
//                                (parseInt(prod_tmp.point) >= $scope.min_price_sku)
//                                &&
//                                (parseInt(prod_tmp.point) <= $scope.max_price_sku)
//                                ) {
//                                // push to new prodlist
//                                prod_list_price_filtered.push(prod_tmp);
//                            }

                        }
                        else {//Added By Hassan Ahamed End

                            if (
                                (parseInt(prod_tmp.price[0].mrp) >= $scope.min_price_sku)
                                &&
                                (parseInt(prod_tmp.price[0].mrp) <= $scope.max_price_sku)
                                ) {
                                // push to new prodlist
                                prod_list_price_filtered.push(prod_tmp);
                            }
                        }


                    }
                    prodList_filtered = prod_list_price_filtered;
                }
                //console.log(prodList_filtered);
                // now filter for discount apply that on prodList_filtered



                // *********************
                $scope.productList = prodList_filtered;

            }
            else {
                $scope.productList = $scope.productList_original;
            }

        };

        $scope.addTocart = function (productDetails) {


            if (productDetails.have_child == 1) {
                state_managerService.refer_url = $location.path();
                var url = "/quickbuy/" + productDetails.id + "/1";
                $location.path(url);
            }
            else {
                $timeout(function () {
                    cartService.addTocart(productDetails);
                    //$scope.cart_size = cartService.get_Cart_Size();
                    $rootScope.$broadcast('cart_changed');
                }, 500);

            }

        };

        $scope.openQuickBuy = function (product_id) {
            state_managerService.refer_url = $location.path();
            $location.path('/quickbuy/' + product_id);
        }


        $scope.enqnow = function (product_id) {
//            state_managerService.refer_url = $location.path();
//            $location.path('/quickbuy/' + product_id);
            $state.go('main.enq',{'product_id':product_id});
        }

    });

//angular.module('exidelife.web')
//    .filter('sort', function() {
//
//    return function (items) {
//        return _.sortBy(items, function (val) {
//            return val;
//        });
//    };
//});