﻿'use strict';

angular.module('exidelife.web')
    .controller('registrationController', function ($scope, $http, $location, $q, registrationService, localStorageService, utilService,$state) {

        $scope.show_loader=false;
       // $scope.isViewLoading = false;
        $scope.registration={};

        $scope.addRegistration = function () {

           // $scope.isViewLoading = true;

            var returnval = '';

            $scope.registration.company = utilService.get_store();

            if ($scope.registration === 'undefined' ){
                alert ("One of the required fields is missing. Please make sure that you have provided all required information!");
                return false;
            }
            else if($scope.registration.first_name == '' || $scope.registration.first_name == null ||typeof $scope.registration.first_name === 'undefined')
            {
                alert('Please Enter First Name');
                return false;
            }
            else if($scope.registration.last_name == '' || $scope.registration.last_name == null || typeof $scope.registration.last_name=== 'undefined' )
            {

                alert('Please Enter Last Name');
                return false;

            }
            else if($scope.registration.mobile_no == '' || $scope.registration.mobile_no == null || typeof $scope.registration.mobile_no=== 'undefined' )
            {
                alert('Please Enter Mobile No');
                return false;

            }
            else if($scope.registration.email_id == '' || $scope.registration.email_id == null || typeof $scope.registration.email_id=== 'undefined' )
            {
                alert('Please Enter E-mail Address');
                return false;

            }
            else if($scope.registration.password == '' || $scope.registration.password == null || typeof $scope.registration.password=== 'undefined' )
            {
                alert('Please Enter Password');
                return false;
            }
            else if ($scope.registration.password !=  $scope.registration.confirm_password )
            {
                alert('Password do not match');
                return false;
            }


            $scope.show_loader=true;

            registrationService.UserRegistration($http, $q, $scope.registration ).then(function (data) {

                    $scope.show_loader=false;
                    if(JSON.parse(data.toString()) == "This Email Is Already Registered" || JSON.parse(data.toString()) == "Failed" || JSON.parse(data.toString()) == "Your Account Activation Is Pending")
                    {
                        alert(JSON.parse(data.toString()));
                    }
                    else{
                        alert(JSON.parse(data.toString()));
                        $state.go("login.login", {});
                    }

                    //  $scope.isViewLoading = false;
                     // $state.go("login.login", {});

                },
                function () {
                    //Display an error message

                    $scope.show_loader=false;
                    $scope.error = error;
                });

        };



    });
