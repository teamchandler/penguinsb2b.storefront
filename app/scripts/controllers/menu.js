'use strict';

angular.module('exidelife.web')
    .controller('MenuCtrl', function ($scope,$state, $http, $q,
                                      utilService,localStorageService,  MenuService) {
        init();
        var menu;
        function init() {
            console.log(utilService.get_store())
            get_all_menu();
        }
        this.isLevel2 = function(val){
            if (val == 2) {
                return true;
            }
            else{
                return false;
            }
        }
        $scope.get_menu = function(name, level){
            //console.log(menuService.get_menu(name,level));
            MenuService.get_menu(name,level);
        }

        $scope.open_level1_page = function(name){
            var url = root_url + "/cat1.html?cat=" + name ;
            $window.location.href = url;
        }

        // new methods//
        function get_all_menu(){
            var expiry_status;
            var last_download_date;
            var tmp_menu =localStorageService.get("menu");
            if (localStorageService.get("menu_refresh")===null)
            {
                last_download_date = ""
            }
            else{
                last_download_date =  localStorageService.get("menu_refresh");
            }
            if (tmp_menu != null){
                // check expiry of menu
                MenuService.get_menu_expiry($http, $q, last_download_date).then(function(data){
                    var expiry_status = data;
                    if (expiry_status > 0) {
                        // get menu again
                        MenuService.get_all_menu($http, $q, utilService.get_store()).then(function(data){
                                menu = data;
                                //console.log(_.filter(menu,function(m){ return m.name === "kids"}));
                                var last_refresh = new XDate();

                                localStorageService.add("menu", menu);
                                localStorageService.add("menu_refresh", last_refresh.toDateString("MMM d, yyyy"));
                                //console.log(new XDate());
                                if (get_app_menu().length !=0) {
                                    $scope.app_link = _.filter(menu,function(m){ return m.name === "apparels"})[0].id;
                                    $scope.l2_app_menu = get_level2_app_menu();
                                    $scope.l3_app_menu = get_level3_app_menu();
                                }
                                if (get_acc_menu().length !=0) {
                                    $scope.acc_link = _.filter(menu,function(m){ return m.name === "accessories"})[0].id;
                                    $scope.l2_acc_menu = get_level2_acc_menu();
                                    $scope.l3_acc_menu = get_level3_acc_menu();
                                }
                                if (get_tog_menu().length !=0) {
                                    $scope.tog_link = _.filter(menu,function(m){ return m.name === "travel & office gear"})[0].id;
                                    $scope.l2_tog_menu = get_level2_tog_menu();
                                    $scope.l3_tog_menu = get_level3_tog_menu();
                                }
                                if (get_pgr_menu().length !=0) {
                                    $scope.pgr_link = _.filter(menu,function(m){ return m.name === "promo gifts & rewards"})[0].id;
                                    $scope.l2_pgr_menu = get_level2_pgr_menu();
                                    $scope.l3_pgr_menu = get_level3_pgr_menu();
                                }
                                if (get_elec_menu().length !=0) {
                                    $scope.elec_link = _.filter(menu,function(m){ return m.name === "electronics & gadgets"})[0].id;
                                    $scope.l2_elec_menu = get_level2_elec_menu();
                                    $scope.l3_elec_menu = get_level3_elec_menu();
                                }
                                if (get_ah_menu().length !=0) {
                                    $scope.ah_link = _.filter(menu,function(m){ return m.name === "appliances & home"})[0].id;
                                    $scope.l2_ah_menu = get_level2_ah_menu();
                                    $scope.l3_ah_menu = get_level3_ah_menu();
                                }
                                if (get_pn_menu().length !=0) {
                                    $scope.pn_link = _.filter(menu,function(m){ return m.name === "premium & novel"})[0].id;
                                    $scope.l2_pn_menu = get_level2_pn_menu();
                                    $scope.l3_pn_menu = get_level3_pn_menu();
                                }
                                if (get_rts_menu().length !=0) {
                                    $scope.rts_link = _.filter(menu,function(m){ return m.name === "ready to ship"})[0].id;
                                    $scope.l2_rts_menu = get_level2_rts_menu();
                                    $scope.l3_rts_menu = get_level3_rts_menu();
                                }
                                if (get_new_menu().length !=0) {
                                    $scope.new_password_link = _.filter(menu,function(m){ return m.name === "new"})[0].id;
                                    $scope.l2_new_menu = get_level2_new_menu();
                                    $scope.l3_new_menu = get_level3_new_menu();
                                }
                                if (get_sp_menu().length !=0) {
                                    $scope.sp_link = _.filter(menu,function(m){ return m.name === "sort by price"})[0].id;
                                    $scope.l2_sp_menu = get_level2_sp_menu();
                                    $scope.l3_sp_menu = get_level3_sp_menu();
                                }



                            },
                            function(){
                                //Display an error message
                                $scope.error= error;
                            });

                    }
                    else {
                        //

                        menu = localStorageService.get("menu")   ;

                        if (get_app_menu().length !=0) {
                            $scope.l2_app_menu = get_level2_app_menu();
                            $scope.l3_app_menu = get_level3_app_menu();
                        }
                        if (get_acc_menu().length !=0) {
                            $scope.l2_acc_menu = get_level2_acc_menu();
                            $scope.l3_acc_menu = get_level3_acc_menu();
                        }
                        if (get_tog_menu().length !=0) {
                            $scope.l2_tog_menu = get_level2_tog_menu();
                            $scope.l3_tog_menu = get_level3_tog_menu();
                        }
                        if (get_pgr_menu().length !=0) {
                            $scope.l2_pgr_menu = get_level2_pgr_menu();
                            $scope.l3_pgr_menu = get_level3_pgr_menu();
                        }
                        if (get_elec_menu().length !=0) {
                            $scope.l2_elec_menu = get_level2_elec_menu();
                            $scope.l3_elec_menu = get_level3_elec_menu();
                        }
                        if (get_ah_menu().length !=0) {
                            $scope.l2_ah_menu = get_level2_ah_menu();
                            $scope.l3_ah_menu = get_level3_ah_menu();
                        }
                        if (get_pn_menu().length !=0) {
                            $scope.l2_pn_menu = get_level2_pn_menu();
                            $scope.l3_pn_menu = get_level3_pn_menu();
                        }
                        if (get_rts_menu().length !=0) {
                            $scope.l2_rts_menu = get_level2_rts_menu();
                            $scope.l3_rts_menu = get_level3_rts_menu();
                        }
                        if (get_new_menu().length !=0) {
                            $scope.l2_new_menu = get_level2_new_menu();
                            $scope.l3_new_menu = get_level3_new_menu();
                        }
                        if (get_sp_menu().length !=0) {
                            $scope.l2_sp_menu = get_level2_sp_menu();
                            $scope.l3_sp_menu = get_level3_sp_menu();
                        }



                    }

                });

            }
            else {
                //console.log("making call to get all menu");
                var company  =   localStorageService.get("user_info").company;
                MenuService.get_all_menu($http, $q,company).then(function(data){
                        menu = data;
                        //console.log(_.filter(menu,function(m){ return m.name === "kids"}));
                        var refresh_date = new XDate();
                        localStorageService.add("menu", menu);
                        localStorageService.add("menu_refresh",refresh_date.toString("MMM d, yyyy"));  //new XDate());
                        //console.log(new XDate());
                        if (get_app_menu().length !=0) {
                            $scope.app_link = _.filter(menu,function(m){ return m.name === "apparels"})[0].id;
                            $scope.l2_app_menu = get_level2_app_menu();
                            $scope.l3_app_menu = get_level3_app_menu();
                        }
                        if (get_acc_menu().length !=0) {
                            $scope.acc_link = _.filter(menu,function(m){ return m.name === "accessories"})[0].id;
                            $scope.l2_acc_menu = get_level2_acc_menu();
                            $scope.l3_acc_menu = get_level3_acc_menu();
                        }
                        if (get_tog_menu().length !=0) {
                            $scope.tog_link = _.filter(menu,function(m){ return m.name === "travel & office gear"})[0].id;
                            $scope.l2_tog_menu = get_level2_tog_menu();
                            $scope.l3_tog_menu = get_level3_tog_menu();
                        }
                        if (get_pgr_menu().length !=0) {
                            $scope.pgr_link = _.filter(menu,function(m){ return m.name === "promo gifts & rewards"})[0].id;
                            $scope.l2_pgr_menu = get_level2_pgr_menu();
                            $scope.l3_pgr_menu = get_level3_pgr_menu();
                        }
                        if (get_elec_menu().length !=0) {
                            $scope.elec_link = _.filter(menu,function(m){ return m.name === "electronics & gadgets"})[0].id;
                            $scope.l2_elec_menu = get_level2_elec_menu();
                            $scope.l3_elec_menu = get_level3_elec_menu();
                        }
                        if (get_ah_menu().length !=0) {
                            $scope.ah_link = _.filter(menu,function(m){ return m.name === "appliances & home"})[0].id;
                            $scope.l2_ah_menu = get_level2_ah_menu();
                            $scope.l3_ah_menu = get_level3_ah_menu();
                        }
                        if (get_pn_menu().length !=0) {
                            $scope.pn_link = _.filter(menu,function(m){ return m.name === "premium & novel"})[0].id;
                            $scope.l2_pn_menu = get_level2_pn_menu();
                            $scope.l3_pn_menu = get_level3_pn_menu();
                        }
                        if (get_rts_menu().length !=0) {
                            $scope.rts_link = _.filter(menu,function(m){ return m.name === "ready to ship"})[0].id;
                            $scope.l2_rts_menu = get_level2_rts_menu();
                            $scope.l3_rts_menu = get_level3_rts_menu();
                        }
                        if (get_new_menu().length !=0) {
                            $scope.new_link = _.filter(menu,function(m){ return m.name === "new"})[0].id;
                            $scope.l2_new_menu = get_level2_new_menu();
                            $scope.l3_new_menu = get_level3_new_menu();
                        }
                        if (get_sp_menu().length !=0) {
                            $scope.sp_link = _.filter(menu,function(m){ return m.name === "sort by price"})[0].id;
                            $scope.l2_sp_menu = get_level2_sp_menu();
                            $scope.l3_sp_menu = get_level3_sp_menu();
                        }
                    },
                    function(){
                        //Display an error message
                        $scope.error= error;
                    });
            }

        }
        function get_app_menu(){
            var tmp = _.filter(menu,function(m){ return m.name === "apparels"});
            if (tmp.length > 0){
                return _.filter(menu,function(m){ return m.name === "apparels"});
            }
            else{
                return [];
            }

        }
        function get_acc_menu(){

            var tmp = _.filter(menu,function(m){ return m.name === "accessories"});
            if (tmp.length > 0){
                return _.filter(menu,function(m){ return m.name === "accessories"});
            }
            else{
                return [];
            }
        }
        function get_tog_menu(){

            var tmp = _.filter(menu,function(m){ return m.name === "travel & office gear"});
            if (tmp.length > 0){
                return _.filter(menu,function(m){ return m.name === "travel & office gear"});
            }
            else{
                return [];
            }
        }
        function get_pgr_menu(){

            var tmp = _.filter(menu,function(m){ return m.name === "promo gifts & rewards"});
            if (tmp.length > 0){
                return _.filter(menu,function(m){ return m.name === "promo gifts & rewards"});
            }
            else{
                return [];
            }
        }
        function get_elec_menu(){

            var tmp = _.filter(menu,function(m){ return m.name === "electronics & gadgets"});
            if (tmp.length > 0){
                return _.filter(menu,function(m){ return m.name === "electronics & gadgets"});
            }
            else{
                return [];
            }
        }
        function get_ah_menu(){

            var tmp = _.filter(menu,function(m){ return m.name === "appliances & home"});
            if (tmp.length > 0){
                return _.filter(menu,function(m){ return m.name === "appliances & home"});
            }
            else{
                return [];
            }
        }
        function get_pn_menu(){

            var tmp = _.filter(menu,function(m){ return m.name === "premium & novel"});
            if (tmp.length > 0){
                return _.filter(menu,function(m){ return m.name === "premium & novel"});
            }
            else{
                return [];
            }
        }
        function get_rts_menu(){

            var tmp = _.filter(menu,function(m){ return m.name === "ready to ship"});
            if (tmp.length > 0){
                return _.filter(menu,function(m){ return m.name === "ready to ship"});
            }
            else{
                return [];
            }
        }
        function get_new_menu(){

            var tmp = _.filter(menu,function(m){ return m.name === "new"});
            if (tmp.length > 0){
                return _.filter(menu,function(m){ return m.name === "new"});
            }
            else{
                return [];
            }
        }
        function get_sp_menu(){

            var tmp = _.filter(menu,function(m){ return m.name === "sort by price"});
            if (tmp.length > 0){
                return _.filter(menu,function(m){ return m.name === "sort by price"});
            }
            else{
                return [];
            }
        }






        function  get_level2_app_menu (){
            return _.where( get_app_menu()[0].menu_items,  {level:2});
        }
        function get_level3_app_menu (){
            return _.where( get_app_menu()[0].menu_items,  {level:3});
        }

        function  get_level2_acc_menu (){
            return _.where( get_acc_menu()[0].menu_items,  {level:2});
        }
        function get_level3_acc_menu (){
            return _.where( get_acc_menu()[0].menu_items,  {level:3});
        }
        function  get_level2_tog_menu (){
            return _.where( get_tog_menu()[0].menu_items,  {level:2});
        }
        function get_level3_tog_menu (){
            return _.where( get_tog_menu()[0].menu_items,  {level:3});
        }
        function  get_level2_pgr_menu (){
            return _.where( get_pgr_menu()[0].menu_items,  {level:2});
        }
        function get_level3_pgr_menu (){
            return _.where( get_pgr_menu()[0].menu_items,  {level:3});
        }
        function  get_level2_elec_menu (){
            return _.where( get_elec_menu()[0].menu_items,  {level:2});
        }
        function get_level3_elec_menu (){
            return _.where( get_elec_menu()[0].menu_items,  {level:3});
        }
        function  get_level2_ah_menu (){
            return _.where( get_ah_menu()[0].menu_items,  {level:2});
        }
        function get_level3_ah_menu (){
            return _.where( get_ah_menu()[0].menu_items,  {level:3});
        }
        function  get_level2_pn_menu (){
            return _.where( get_pn_menu()[0].menu_items,  {level:2});
        }
        function get_level3_pn_menu (){
            return _.where( get_pn_menu()[0].menu_items,  {level:3});
        }

        function  get_level2_rts_menu (){
            console.log(get_rts_menu()[0].menu_items);
            return _.where( get_rts_menu()[0].menu_items,  {level:2});
        }
        function get_level3_rts_menu (){
            return _.where( get_rts_menu()[0].menu_items,  {level:3});
        }
        function  get_level2_new_menu (){
            return _.where( get_new_menu()[0].menu_items,  {level:2});
        }
        function get_level3_new_menu (){
            return _.where( get_new_menu()[0].menu_items,  {level:3});
        }
        function  get_level2_sp_menu (){
            return _.where( get_sp_menu()[0].menu_items,  {level:2});
        }
        function get_level3_sp_menu (){
            return _.where( get_sp_menu()[0].menu_items,  {level:3});
        }
    });
