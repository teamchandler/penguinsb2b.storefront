/**
 * Created by sweta on 6/24/2014.
 */


'use strict';

angular.module('exidelife.web')
    .controller('contactusCtrl', function ($http,$q,$location,$scope,state_managerService,contactusService,utilService,localStorageService) {
        console.log("home")

        $scope.show_loader=false;
        $scope.city_list = utilService.get_city_list();

        init();
        function init() {

            // alert("ok");

        }

        $scope.SaveEnquireRecord = function () {


            if ($scope.enquire == "" || typeof $scope.enquire === 'undefined' )
            {
                alert ("One of the required fields is missing. Please make sure that you have provided all required information!");
                return false;
            }

            else if ($scope.enquire.name == "" || typeof $scope.enquire.name === 'undefined' )
            {
                alert ("One of the required fields is missing. Please make sure that you have provided all required information!");
                return false;
            }

            else if ($scope.enquire.mobile_no == "" || typeof $scope.enquire.mobile_no === 'undefined' )
            {
                alert ("One of the required fields is missing. Please make sure that you have provided all required information!");
                return false;
            }
            else if ($scope.enquire.email == "" || typeof $scope.enquire.email === 'undefined' )
            {
                alert ("One of the required fields is missing. Please make sure that you have provided all required information!");
                return false;
            }
            else if (($scope.enquire.general_information == "" || typeof $scope.enquire.general_information === 'undefined' )&&($scope.enquire.product_code == "" || typeof $scope.enquire.product_code === 'undefined' ) && ($scope.enquire.additional_remarks == "" || typeof $scope.enquire.additional_remarks === 'undefined' ) )
            {
                alert("One of the required fields is missing. Please make sure that you have provided all required information!");
                return false;
            }




            var enquire_from = utilService.get_store();
            $scope.enquire.enquire_from = enquire_from;
            $scope.enquire.created_by = localStorageService.get('user_info').email_id;

            $scope.show_loader=true;

            contactusService.insertenquireinfo($http, $q, $scope.enquire).then(function (data) {
                    //   alert(JSON.parse(data.toString()));

                    alert("Your enquiry is submitted, find the status of your enquiry in My account Page");

                    state_managerService.refer_url = $location.path();



                    $location.path('/thankyou');
                },
                function () {
                    //Display an error message
                    $scope.error = error;
                });

        }


    });
