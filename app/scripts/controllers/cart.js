'use strict';

angular.module('exidelife.web')
  .controller('CartCtrl', function ($scope, $state, $http, $q, $timeout , $window,
                                    NotificationService, cartService,
                                    utilService, localStorageService) {


        var get_cart = function(){
            $scope.cart_size = cartService.get_cart_size();
             $scope.cart = cartService.get_cart();
        }
		
		$scope.total_order_amount = cartService.get_cart_amount() ;


        $scope.$on('cart added', function() {

            get_cart();
            var tmp = $scope.cart;
            //console.log(tmp);
            apply_calculation(tmp);

            if ((localStorageService.get('local_cart') != null)
                &&
                (localStorageService.get('local_cart') != "")
                ) {

                cartService.save_cart_to_db($http, $q).then(function (data) {
                        //alert("Cart is updated successfully");
                        alert('Item added to the cart');
                    },
                    function () {
                        //Display an error message
                        $scope.error = error;
                    });
            }

        });

        $scope.$on('cart changed', function() {


            get_cart();
            var tmp = $scope.cart;
            // console.log(tmp);
            apply_calculation(tmp);
            if ((localStorageService.get('local_cart') != null)
                &&
                (localStorageService.get('local_cart') != "")
                ) {

                cartService.save_cart_to_db($http, $q).then(function (data) {
                       // alert("Cart is updated successfully");
                    },
                    function () {
                        //Display an error message
                        $scope.error = error;
                    });
            }

        });

        $scope.$on('cart blank', function() {
            get_cart();
        });


        var init = function(){
           // localStorageService.set('local_cart', "[]");
            get_cart();
            var tmp = $scope.cart;
            //console.log(tmp);
            apply_calculation(tmp);
        }

        $scope.delete_from_cart = function (sel_cart_item) {
          //  console.log(sel_cart_item)
            $timeout(function () {
                cartService.delete_from_cart(sel_cart_item);
                NotificationService.cart_changed("cart changed", sel_cart_item)
            }, 1000);
        }

        var apply_calculation = function (tmp) {
           // var conv_ratio = utilService.get_conversion_ratio();
           // var store_type = utilService.get_store_type();
           //  $scope.store_type = store_type;
           // $scope.conv_ratio = conv_ratio;
            //console.log($scope.store_type) ;

            var cart_list=tmp;
            var total_order_amt=0;
            if(cart_list.length>=0){
                angular.forEach(cart_list, function(item) {
                    total_order_amt += parseInt(item.order_amount);
                });
                if(total_order_amt==0){
                    $scope.total_order_amount="0";
                }
                else
                {
                    $scope.total_order_amount=total_order_amt;
                }

            }

        }

        $scope.changeorderqty = function (sel_cart_item) {

            var prod_ord_qty=[];
            prod_ord_qty= sel_cart_item.child_prod_detail;
            var total_ord_qty = 0;
            angular.forEach(prod_ord_qty, function(item) {
                if(item.ord_qty!="" && item.ord_qty!=null)
                {
                    if(item.ord_qty<item.current_stock){
                        total_ord_qty += parseInt(item.ord_qty);
                    }
                    else
                    {
                        item.ord_qty="";
                        alert('This number of quantity is not available');
                    }
                }
            });

            if(total_ord_qty<=0){

                alert('Minimum number of quantity should be 1');
            }
            else
            {
                sel_cart_item.order_quantity=total_ord_qty;
            }

            var prod_price_list=sel_cart_item.parent_prod_detail.price;
            if(prod_price_list.length>0) {
                var prod_price=0;
                if(total_ord_qty>0){
                    angular.forEach(prod_price_list, function(item) {
                        if(total_ord_qty==item.min_qty || total_ord_qty==item.max_qty || (total_ord_qty>item.min_qty && total_ord_qty<item.max_qty))
                        {
                            prod_price = parseInt(item.list);
                        }
                    });
                    sel_cart_item.order_amount=prod_price*total_ord_qty;
                }
                else
                {
                    sel_cart_item.order_quantity="";
                    sel_cart_item.order_amount="";
                }
            }

            console.log(sel_cart_item);

            $timeout(function () {
                cartService.addTocart(sel_cart_item,$http, $q);
                NotificationService.cart_changed("cart changed", sel_cart_item)
            }, 1000);



        }





        $scope.triggerCheckOut = function () {

         //  alert('Hi');

            var cart_list=$scope.cart;
            var total_order_amt=0;
            var cond=true;
            if(cart_list.length>0){
                angular.forEach(cart_list, function(item) {
                    total_order_amt += parseInt(item.order_amount);

                    if(item.order_quantity<1|| item.order_quantity==""||item.order_quantity==null ){
                        alert('Item quantity should be greater than 1')
                        cond= false;

                    }

                });

            }else
            {
                alert('Select an item first');
                return false;
            }

            if(cond){
                $state.go('main.ship',{});
                //  $window.location.href = root_url+"/index.html#/main/ship";
            }


        }



        init();
  });
