'use strict';

angular.module('exidelife.web')
  .controller('LoginCtrl', function (
                            $scope, $http, $q, $window, $location, $rootScope, $state,
                            loginService,  localStorageService, cartService, utilService
                                    )
    {
        $scope.$on("login_success", function () {
            // This function listens to any change in scope for broadcast message login_success.
            // If it gets that, applies subsequent code
            set_login_text();
        });
        var set_login_text = function () {
            //        console.log(localStorageService.get('user_info').first_name == null )   ;
            if ((localStorageService.get('user_info') != null)) {
                if (localStorageService.get('user_info').first_name != null) {
                    var user_info = localStorageService.get('user_info');
                    $scope.signin_text = ("welcome " + user_info.first_name + " " + user_info.last_name).toTitleCase();
                    $scope.signout_text = "Sign Out";
                }
                else {
                    utilService.change_state($state, "login.login", {});
                }

            }
            else {

                    utilService .change_state($state, "login.login", {});

            }
        }

        init();


        function init() {
            //check_login();
            var user_info;
            set_login_text();
        }

        $scope.opts = {
            backdropFade: true,
            dialogFade: true
        };

        $scope.openRegistration = function () {
            localStorageService.set('user_info', "");
            localStorageService.set('local_cart', "[]");
            localStorageService.set('local_wishlist', "[]");

//            state_managerService.refer_url = $location.path();
            var url = root_url + "/login/login.html#/registration";
            console.log(url);
            $window.location.href = url;
        }
        $scope.openLogin = function () {

            localStorageService.set('user_info', "[]");
            localStorageService.set('local_cart', "");
            localStorageService.set('local_wishlist', "");

//            state_managerService.refer_url = $location.path();
//            var login_url = root_url + "/#/login/login";
//            $window.location.href = login_url;

            $state.go('login.login',{});
        }
        $scope.execLogout = function () {
            var url;
            $window.location.href = login_url;
            if (localStorageService.get('user_info') != null) {
                localStorageService.add('user_info', "");
                this.openLogin();
            }
            else {
                localStorageService.add('user_info', "");
                this.openRegistration();
                //            url =  root_url + "/index.html#/registration" ;
                //            $window.location.href = url;

            }
        }



    });
