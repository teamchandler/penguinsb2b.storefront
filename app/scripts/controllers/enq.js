/**
 * Created by sweta on 6/21/2014.
 */
'use strict';
angular.module('exidelife.web').controller('enqCtrl', function ($scope, $http, $q, $location, $stateParams,$rootScope, enqService,productService,
                                                 localStorageService, utilService) {



    $scope.show_loader=false;
//    var product_Id = $routeParams.prodId;


    var email_id =  localStorageService.get('user_info').email_id;
    var product_id = $stateParams.product_id;
    $scope.city_list = utilService.get_city_list();
    $scope.product_id = product_id;

    $scope.sizes = [];


    //$scope.child_product;


    init();


    var prod_view = {
        user_id: "",
        id: "",
        sku: "",
        brand: "",
        Name: "",
        price: "",
        Express: ""
    }

    function init() {

        var product_details, parent_product, child_product;
        child_product = [];
        productService.get_productDetailsById($http, $q, product_id).then(function (data) {
                product_details = data;
                var prod_name = product_details[0];
                $scope.product_name = JSON.parse(prod_name).Name;
                $scope.product_all_data = JSON.parse(prod_name);
                $scope.product_mrp = $scope.product_all_data.price[0].mrp;
                if (product_details.length > 1) {
                    $scope.size_ind = true;
                    for (var i = 1; i < product_details.length; i++) {
                        var child_prod_detail=[];


                       // var child_prod_stock=[];
                        child_prod_detail=JSON.parse(product_details[i]);
                        child_prod_detail.name =  $scope.product_name;
                        delete child_prod_detail.stock;
                       // var value_to_remove = stock;
                        //  var removed = child_prod_detail.pop();
                       // child_prod_stock=JSON.parse(JSON.parse(product_details[i]).stock);
//                        if(child_prod_stock.length>0){
//                            var total_stock = 0;
//                            angular.forEach(child_prod_stock, function(item) {
//                                total_stock += item.stock;
//                            });
//                        }

                      //  child_prod_detail.stock=child_prod_stock;
                        child_prod_detail.ord_qty="";
                        child_product.push(child_prod_detail);

                    }
                    console.log (child_product);
//                    if (child_product.length > 0) {
//                        $scope.out_of_stock = 0;
//                        $scope.stock_msg = "In stock";
//                    }
//                    else {
//                        $scope.out_of_stock = 1;
//                        $scope.stock_msg = "Out of stock";
//                    }

                }
                else {
                    $scope.size_ind = false;
                  //  var stock = JSON.parse(parent_product).stock;
                    //console.log(stock);
//                    if (stock <= 0) {
//                        $scope.out_of_stock = 1;
//                        $scope.stock_msg = "Out of stock";
//                    }
//                    else {
//                        if ((stock <= 10) && (stock > 0)) {
//                            $scope.out_of_stock = 0;
//                            $scope.stock_msg = "Only " + stock.toString() + " left";
//                        }
//                        else {
//                            $scope.out_of_stock = 0;
//                            $scope.stock_msg = "In stock";
//                        }
//                    }
                }
                $scope.sizes = [];
                console.log($scope.sizes);
                $scope.child_product=child_product;
            },
            function () {
                //Display an error message
                $scope.error = "Product Data not found";
            });
        $scope.$watch('myModel', function (v) {
            //console.log('changed', v);
            $scope.selected_prod = v;
            //alert (v);
        });

    }

    $scope.cal_total_qty = function () {

        var prod_enq_qty=[];
        prod_enq_qty= $scope.child_product;
        var total_enq_qty = 0;
        angular.forEach(prod_enq_qty, function(item) {

            if(item.ord_qty!="" && item.ord_qty!=null)
            {
                total_enq_qty += parseInt(item.ord_qty);
            }
        });
        $scope.total_enq_qty=total_enq_qty;
        $scope.total_enq_qty_wise_amount = (total_enq_qty * $scope.product_mrp) * 1 ;

    }

    $scope.cal_total_enq_qty = function () {

        var prod_enq_qty=[];
        prod_enq_qty= $scope.child_product;
        angular.forEach(prod_enq_qty, function(item) {

           item.ord_qty="" ;

        });

    }

    $scope.SaveEnquireRecord = function () {


        var size_quantity = [];
        var enq_details= JSON.stringify($scope.child_product);

        if ($scope.enquire == "" || typeof $scope.enquire === 'undefined' )
        {
            alert ("One of the required fields is missing. Please make sure that you have provided all required information!");
            return false;
        }
        else if ( $scope.total_enq_qty == ""  || typeof $scope.total_enq_qty === 'undefined' || $scope.total_enq_qty ==null){
            alert ("Please Enter Total Quantity");
            return false;
        }
        else if ( $scope.enquire.logo == ""  || typeof $scope.enquire.logo === 'undefined'){
            alert ("One of the required fields is missing. Please make sure that you have provided all required information!");
            return false;
        }
        else if ( $scope.enquire.email_id == ""  || typeof $scope.enquire.email_id === 'undefined'){
            alert ("One of the required fields is missing. Please make sure that you have provided all required information!");
            return false;
        }
        else if ( $scope.enquire.city == ""  || typeof $scope.enquire.city === 'undefined'){
            alert ("One of the required fields is missing. Please make sure that you have provided all required information!");
            return false;
        }

        else
        {
            $scope.show_loader=true;

            var enquire_from = utilService.get_store();
            $scope.enquire.prod_id = $scope.product_id;
            $scope.enquire.prod_name = $scope.product_name;
            $scope.enquire.enquire_from = enquire_from;
            $scope.enquire.size_quantity = enq_details;
            $scope.enquire.created_by = email_id;
            $scope.enquire.total_enq_qty = $scope.total_enq_qty;
            $scope.enquire.total_enq_qty_wise_amount = $scope.total_enq_qty_wise_amount;
            $scope.enquire.store=utilService.get_store();


          //  $scope.enquire.size = $scope.size;

            enqService.insertenquireinfo($http, $q, $scope.enquire).then(function (data) {
                    // alert(JSON.parse(data.toString()));
                    //  state_managerService.refer_url = $location.path();
                    alert('Your enquiry is submitted, find the status of your enquiry in My account Page');

                    $location.path('/thankyou');
                },


                function () {
                    //Display an error message
                    $scope.error = error;
                })
        }
    }

});

