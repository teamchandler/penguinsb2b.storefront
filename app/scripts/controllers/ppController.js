/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 20/10/13
 * Time: 1:08 PM
 * To change this template use File | Settings | File Templates.
 */

'use strict';

angular.module('exidelife.web')
.controller('ppController', function ($scope, $http, $q, $location, $window, $state,$stateParams, $rootScope, utilService, PpService, localStorageService, cartService,acctService, giftService,NotificationService) {
                                         

    //I like to have an init() for controllers that need to perform some initialization. Keeps things in
    //one place...not required though especially in the simple example below
   //console.log("home");
	
    $scope.isClickable = false;

    $scope.check_details={};



        /************** Start Test Calender Panel*****************/
        $scope.today = function() {
            $scope.check_details.date = new Date();
        };
        $scope.today();

        $scope.clear = function () {
            $scope.check_details.date = null;
        };

        // Disable weekend selection
        $scope.disabled = function(date, mode) {
            return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
        };

        $scope.toggleMin = function() {
            $scope.minDate = $scope.minDate ? null : new Date();
        };
        $scope.toggleMin();

        $scope.open = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened = true;
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };

        $scope.initDate = new Date('2016-15-20');
        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];

        /************** End Test Calender Panel*****************/

        function check_login() {
        if (localStorageService.get('user_info') == null) {
            $window.location.href = root_url + "/index.html#/login";
        }
        else {
            $window.location.href = root_url + "/home.html";
        }
    }
    $scope.select_address = function (selected_address) {
        populate_selected_address(selected_address);
    }

    $scope.RedeemPoint = function () {

        
        var returnval = '';
        localStorageService.set ("express_shipping", 0);
        $scope.isClickable = true;
        if ($scope.balance_amount <= 0) {
            PpService.RedeemUserPoint($http, $q, $scope.pp).then(function (data) {
                acctService.set_points($scope.pp.Points); // change point balance
                localStorageService.set('local_cart', "[]");//reset cart
                var confirm_order_url = root_url + "/payment/payment.html#/payment/2";
                $window.location.href = confirm_order_url;
            },
                function () {
                    //Display an error message
                    $scope.error = error;
                });
        }
        else {
            //alert (payment_gateway)  ;
            if (payment_gateway === 0){
                alert ("Please use your points to make payment")
            }
            else {
                $('#myform').submit();
            }
        }
    }

    var order_id;
	var Payment_mode;
    var check = [];
    var count = 0;
    var check_num;
    $scope.check_data = [];
     var chck_no = 0;
    var gv = {
        id: "",
        gift_certificate_code: "",
        amount: 0
    }
    var gv_list = [];
    for (var i = 1; i <= 5; i++) {
        var g = {
            id: i.toString(),
            amount: 0
        };//new gv;//.gift_certificate_code= i.toString();
        gv_list.push(g);
    }
    //console.log(gv_list);
    $scope.gv_list = gv_list;
    var conv_ratio = utilService.get_conversion_ratio();
    $scope.conv_ratio = conv_ratio;
    $scope.store_type = utilService.get_store_type();



    function init() {
        //        console.log ($(document.location));

        // get order_id

        console.log('New PP Controller.')


        order_id = $stateParams.order_id;
        if  (localStorageService.get('express_shipping') != null)
        {
            $scope.express_shipping  =localStorageService.get('express_shipping');
        }
        else {
            $scope.express_shipping  = 0;
        }
//        if (cartService.express_shipping){
//            $scope.express_shipping = 150; // todo - put to config
//        }
//        else {
//            $scope.express_shipping = 0;
//        }


        $scope.payment_gateway = utilService.get_payment_gateway();

        // get selected address
        $scope.selected_address = localStorageService.get('selected_ship_address');
        $scope.shipping_address = $scope.selected_address.address_populated;
        $scope.shipping=localStorageService.get('selected_ship_address').shipping_amount;
        $scope.gv_total = 0;


        $scope.user_point = parseInt(acctService.get_points()).toFixed(0);


        $scope.point_amount = ($scope.user_point / conv_ratio).toFixed(0);
        $scope.redeem_point = 0;

        $scope.redeem_point_rs = 0;//($scope.redeem_point  / conv_ratio).toFixed(0)
        $scope.total_cart_amount = cartService.get_cart_amount().toFixed(0)*1;

       // $scope.balance_amount = $scope.total_cart_amount + parseInt($scope.express_shipping);
        $scope.balance_amount = $scope.total_cart_amount ;

        $scope.discount_coupon_total = 0;
        $scope.discount_coupon = "";

        // pre-populate with point available
        // added by rahul on 13th Feb
        if ($scope.point_amount >= $scope.balance_amount){
            $scope.redeem_point =  $scope.balance_amount;
        }
        else {
            $scope.redeem_point =  $scope.point_amount;
        }
        $scope.redeem_point_rs =  ($scope.redeem_point / conv_ratio).toFixed(0);
        console.log('total_cart_amount'+ $scope.total_cart_amount );

        console.log('redeem_point_rs'+ $scope.redeem_point_rs );

        console.log('Shipping'+ $scope.express_shipping );





        $scope.balance_amount = $scope.total_cart_amount * 1+ parseInt($scope.express_shipping) - $scope.redeem_point_rs;

        console.log('Balance_amount'+ $scope.balance_amount );

        $scope.pp = {};
//        $scope.redeem_points();
        calc_balance();

        $scope.pp.total_amount = $scope.balance_amount;//cartService.get_cart_amount();
        $scope.pp.actual_amount = $scope.total_cart_amount;
        $scope.pp.Order_Id = $stateParams.order_id;
        $scope.pp.Points = $scope.redeem_point;
        $scope.pp.Points_Inr = $scope.redeem_point_rs;
        $scope.pp.billing_cust_name = $scope.selected_address.billing_name; // billing start
        $scope.pp.billing_cust_address = $scope.selected_address.address;
        $scope.pp.billing_cust_state = $scope.selected_address.state;
        $scope.pp.billing_cust_tel = $scope.selected_address.mobile_no;
        $scope.pp.billing_cust_email = localStorageService.get('user_info').email_id;
        $scope.pp.billing_cust_city = $scope.selected_address.city;
        $scope.pp.billing_zip_code = $scope.selected_address.pin;
        $scope.pp.name = $scope.shipping_address.fullname;  // shipping start
        $scope.pp.address = $scope.shipping_address.address;
        $scope.pp.state = $scope.shipping_address.state;
        $scope.pp.mobile_number = $scope.shipping_address.mobile_no;
        $scope.pp.delivery_cust_city = $scope.shipping_address.city;
        $scope.pp.delivery_zip_code = $scope.shipping_address.zipcode;
    }
    var selected_address = {};
    var apply_rules = function(discount){



        if ( discount == null){
            $scope.discount_validation_msg = "Coupon not valid."  ;
            $scope.discount_coupon_total = 0;
        }
        else {
            if (discount.min_value <=$scope.total_cart_amount ){
                if (discount.discount_type == "f"){
                    // flat off
                    $scope.discount_coupon_total = discount.discount_value;
                }
                else {
                    // % off
                    var off =  ( $scope.total_cart_amount *  (1-((100-discount.discount_value)/100))).toFixed(0);
                    //console.log(discount.max_discount);
                    if (off <= discount.max_discount){
                        $scope.discount_coupon_total = off;
                    }
                    else {
                        $scope.discount_coupon_total =    discount.max_discount;
                    }
                }

            }
            else {
                $scope.discount_validation_msg = "Min purchase criteria not met"  ;
            }
        }


    }
    init();
    $scope.validate_gv = function (gv, idx) {
        // first check for dup
        //var tmp = _.where($scope.gv_list, {gift_voucher_code:gv})  ;
        var l_found_count = 0;
        for (var i = 0; i < $scope.gv_list.length; i++) {
            if ($scope.gv_list[i].gift_certificate_code === gv) {
                l_found_count = l_found_count + 1;
            }
        }

        if (l_found_count > 1) {
            $scope.msg = "Duplicate Gift Voucher";
        }
        else {
            // check for validity
            $scope.msg = "";
            giftService.validate_gv($http, $q, gv).then(function (data) {

                $scope.gv_list[idx] = data;
                $scope.gv_list[idx].gift_certificate_code = gv;
                $scope.gv_list[idx].company = utilService.get_store();
                console.log($scope.gv_list);
                if ($scope.gv_list[idx].amount == 0) {
                    $scope.msg = "Invalid Gift Voucher";
                }
                //console.log($scope.gv_list)  ;
                calc_balance();
            },
                function () {
                    //Display an error message
                    $scope.error = error;
                });
        }
    }

        var discount_coupon_validation = function(){
        if ($scope.discount_coupon ===""){
            $scope.discount_coupon_total = 0;
            $scope.discount_validation_msg = ""  ;
            calc_balance();
        }
        else{
        PpService.validate_discount_coupon($http, $q, $scope.discount_coupon, utilService.get_store(), localStorageService.get('user_info').email_id).then(function (data) {
                console.log(data);
                var discount = JSON.parse(data.rule);
                apply_rules(discount);
                //console.log($scope.discount_validation_msg);
                //console.log($scope.gv_list)  ;
                calc_balance();
            },
            function () {
                //Display an error message
                $scope.error = error;
            });
        }
    }
    $scope.apply_coupon = function(){
       discount_coupon_validation();
    }


    $scope.validate_discount_point = function () {
        discount_coupon_validation();

    }

    $scope.Add_logo_charges = function () {
        $scope.logo_charges_added =  parseInt($scope.logo_charges);

        $scope.balance_amount = (  parseInt($scope.balance_amount*1) + parseInt($scope.logo_charges) );

        console.log('Balance_amount_after Logo '+ $scope.balance_amount );

      }

    $scope.redeem_points = function () {

        //console.log("redeem_point");
        console.log('redem_point method fire')
        if (parseInt($scope.redeem_point) > parseInt($scope.user_point)) {
            $scope.redeem_point = $scope.user_point
        }
        calc_balance();
        //        var old = $scope.old_redeem_point;
        //        calc_balance();
        //        if ($scope.balance_amount <= 0){
        //            $scope.redeem_point =  $scope.old_redeem_point;
        //        }
        console.log('redem_point method fire')

    }
    $scope.remove_gv = function (idx) {
        $scope.gv_list[idx] = { id: idx, gift_certificate_code: "", amount: 0 };
        calc_balance();
        //console.log($scope.gv_list);


    }

    function calc_balance() {
        var certcode = '';
        $scope.redeem_point_rs = ($scope.redeem_point / conv_ratio).toFixed(0);
        $scope.total_cart_amount = cartService.get_cart_amount().toFixed(0);

        var gv_total = 0;
        for (var i = 0; i < 5; i++) {

            if ($scope.gv_list[i].amount != null) {
                gv_total = gv_total + $scope.gv_list[i].amount;

                if (certcode == '') {
                    if ($scope.gv_list[i].gift_certificate_code != null)// || typeof($scope.gv_list[i].gift_certificate_code) != 'undefined') {
                    {
                        certcode = $scope.gv_list[i].gift_certificate_code;
                    }
                }
                else {
                    if ($scope.gv_list[i].gift_certificate_code != null)  // || typeof($scope.gv_list[i].gift_certificate_code) != 'undefined') {
                    {
                        certcode = certcode + '|' + $scope.gv_list[i].gift_certificate_code;
                    }
                }
            }
        }

        $scope.gv_total = gv_total;
        //if ($scope.discount_coupon_total != 0){

        $scope.balance_amount = $scope.total_cart_amount*1  + parseInt($scope.express_shipping) -$scope.discount_coupon_total - $scope.redeem_point_rs - gv_total;



        if ($scope.balance_amount < 0){
                if ($scope.redeem_point_rs >= $scope.discount_coupon_total ){
                    $scope.redeem_point_rs =  $scope.redeem_point_rs - $scope.discount_coupon_total;
                    $scope.redeem_point = ($scope.redeem_point_rs * conv_ratio);

                }
//                else {
//                    $scope.redeem_point_rs = 0;
//                    $scope.redeem_point = 0;
//                }
            }

//        }
        $scope.balance_amount = $scope.total_cart_amount*1 + parseInt($scope.express_shipping) -$scope.discount_coupon_total - parseInt($scope.redeem_point_rs) - gv_total;



        if ($scope.balance_amount < 0) //For -ve balance
            $scope.balance_amount = 0;

       // $scope.old_redeem_point = $scope.redeem_point;
        $scope.pp.total_amount = $scope.balance_amount;
        $scope.pp.Points = $scope.redeem_point;
        $scope.pp.Points_Inr = $scope.redeem_point_rs;
        $scope.pp.EGift_VNO = certcode;
        $scope.pp.EGift_Amt = gv_total;
        $scope.pp.discount_coupon = $scope.discount_coupon;
        $scope.pp.discount_coupon_total = $scope.discount_coupon_total;

        //console.log($scope.pp);

    }

    $scope.route_to = function (page) {
        var url = "/" + page + "/" + order_id;
        $location.path(url);
    }
	
	 $scope.check_details;
	 
	 
	  var Payment_mode1 = $scope.payment_details;
	  var test = Payment_mode1;
	 
	 
	 
	 $scope.open_neft_popup = function()
	 {
         $scope.neft_pop=true;
	   //$window.location.href = root_url + "/index.html#/main/bank_details/" + order_id;
//         $state.go('main.bank_details',{'order_id':order_id});


	 }
	 $scope.open_cheque_popup = function()
	 {

         $scope.cheque_pop=true;

//	   $window.location.href = root_url + "/index.html#/main/payment_details/" + order_id;
//       $state.go('main.payment_details',{'order_id':order_id});
	 
	 }
	 
	
	$scope.checkout = function(){
	
		 
	 // $window.location.href = root_url + "/index.html#/main/checkout/" + order_id;
	  
	
	}


        $scope.save_bank_details_data = function(){


            $scope.transact_details.order_id = order_id;
            $scope.transact_details.payment_mode = "2";
            if ($scope.transact_details === 'undefined' ){
                alert ("One of the required fields is missing. Please make sure that you have provided all required information!");
                return false;
            }

            else if  ($scope.transact_details.bank_name == '' || typeof $scope.transact_details.bank_name  === 'undefined'  ){
                alert ("One of the required fields is missing. Please make sure that you have provided all required information!");
                return false;
            }

            else if ($scope.transact_details.transaction_no== "" || typeof $scope.transact_details.transaction_no=== 'undefined' ){
                alert ("One of the required fields is missing. Please make sure that you have provided all required information!");
                return false;
            }

           else if ($scope.transact_details.date== "" || typeof $scope.transact_details.date=== 'undefined' ){
                alert ("One of the required fields is missing. Please make sure that you have provided all required information!");
                return false;
            };


            $scope.show_loader=true;

            PpService.add_transaction_details($http, $q, $scope.transact_details).then(function(data){  // todo config
                    //Update UI using data or use the data to call      another service

                    // o_id  = data;
                    $scope.show_loader=false;
                    localStorageService.add('local_cart', []);
                    NotificationService.cart_blank("cart blank");
                    alert('Thanks for your order.Your order details has been recorded ');
                    $scope.neft_pop=false;
                    $state.go('main.home');

                },
                function(){
                    //Display an error message
                    $scope.show_loader=false;
                    $scope.error= error;
                });

        }

        $scope.select_delete = function(c_no)
        {
            chck_no = c_no;

           // return serial_no;

        }
        $scope.delete_check = function()
        {
            if(chck_no == '' || chck_no === 'undefined' )
            {
              alert("Please select a cheque to Remove");

            }
            else
            {

                for(var k=0;k<$scope.check_data.length;k++) {
                    var c_num =  chck_no;
                    var c_data = $scope.check_data[k].check_number;
                    if(c_num == c_data )
                    {
                        $scope.check_data.splice(k , 1);
                        // delete $scope.check_data[k];
                    }

                }

                for(var k=0;k<$scope.check_data.length;k++) {
                        $scope.check_data[k].sln_no=k+1;
                }

            }

        }

        $scope.save_check = function()
        {

            $scope.check_details.order_id = order_id ;
            $scope.check_details.payment_mode = "1";


            if ($scope.check_details === 'undefined' ){
            alert ("One of the required fields is missing. Please make sure that you have provided all required information!");
            return false;
            }

           else if ($scope.check_details.check_no == '' || typeof $scope.check_details.check_no  === 'undefined'  ){
                alert ("One of the required fields is missing. Please make sure that you have provided all required information!");
                return false;
            }

            else if ($scope.check_details.bank_name== "" || typeof $scope.check_details.bank_name=== 'undefined' ){
                alert ("One of the required fields is missing. Please make sure that you have provided all required information!");
                return false;
            }

            else if ($scope.check_details.date== "" || typeof $scope.check_details.date=== 'undefined' ){
                alert ("One of the required fields is missing. Please make sure that you have provided all required information!");
                return false;
            };


//            function addOne(){
//                count++;
//                return count;
//            }
            $scope.check = {};
           // $scope.check= $scope.check_details;
            $scope.check.check_number = $scope.check_details.check_no;
            $scope.check.banks_name = $scope.check_details.bank_name;
            $scope.check.trn_date = $scope.check_details.date;
            $scope.check.order_id =  $scope.check_details.order_id;
            $scope.check.payment_mode =  $scope.check_details.payment_mode;
            $scope.check.company = utilService.get_store();

            if($scope.check_data== "" ||$scope.check_data== null || typeof $scope.check_data=== 'undefined'){
                $scope.check.sln_no = 1;
            }
            else{
                $scope.check.sln_no = ($scope.check_data.length *1)+1;
            }

            for(var j=0;j<$scope.check_data.length;j++) {
                if($scope.check.check_number==$scope.check_data[j].check_number){
                    alert("Same Cheque Number Is Not Allowed");
                    return false;
                }
              }

                $scope.check_data.push($scope.check);

                $scope.check_details.check_no = "";
                $scope.check_details.bank_name = "";
                $scope.check_details.date = "";

        }

        $scope.save_check_details_data = function(){


            $scope.check_details.order_id = order_id ;
            $scope.check_details.payment_mode = "1";




//            for(var j=0;j< $scope.check_data.length;j++) {
//                $scope.check_details.sln_no = $scope.check_data[j].sln_no;
//                $scope.check_details.check_no = $scope.check_data[j].check_number;
//                $scope.check_details.bank_name = $scope.check_data[j].banks_name;
//                $scope.check_details.date = $scope.check_data[j].trn_date;
//
//            }



            $scope.show_loader=true;
                PpService.add_check_details($http, $q, $scope.check_data).then(function (data) {       // todo config
                    //Update UI using data or use the data to call      another service


                        $scope.show_loader=false;
                        localStorageService.add('local_cart', []);
                        NotificationService.cart_blank("cart blank");
                        alert('Thanks for your order.Your order details has been recorded ');
                        $scope.cheque_pop = false;
                        $state.go('main.home');

//                o_id  = data;
                    },
                    function () {
                        //Display an error message
                        $scope.show_loader=false;
                        $scope.error = error;
                    });


        }

        $scope.neft_popup_close = function(){
            //cartService.express_shipping = false;
            //$rootScope.express_shipping = 0;
           // localStorageService.add("express_shipping", 0);
            $scope.neft_pop=false;
        }

        $scope.cheque_popup_close = function(){
            //cartService.express_shipping = false;
            //$rootScope.express_shipping = 0;
            // localStorageService.add("express_shipping", 0);
            $scope.cheque_pop=false;
        }

});