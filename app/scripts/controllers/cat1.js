'use strict';

angular.module('exidelife.web')
  .controller('Cat1Ctrl', function ($scope, $state, $stateParams,
                                    $location, $http, $q,
                                    adService, utilService, localStorageService) {
        var slides = $scope.slides = [];
        var ads;// = adService.get_ads();

        $scope.templates =
            [
                { id:'1.0',  name: 'cat1_ad_page_static', url: '/Content/ad/cat1-1.0.html' },
                { id:'2.0',  name: 'cat1_ad_page_static', url: '/Content/ad/cat1-2.0.html' },
                { id:'3.0',  name: 'cat1_ad_page_static', url: '/Content/ad/cat1-3.0.html' }
            ]

        //$scope.template = $scope.templates[0];
        $scope.addSlide = function(img_link,  img_href) {
            var newWidth = 300;// + ((slides.length + (25 * slides.length)) % 150);
            slides.push({
                image: img_link ,  //'http://placekitten.com/' + newWidth + '/200'
                href: img_href

            });
        };
        function check_login (){
            if (localStorageService.get('user_info') == null){
                $location.path("/login");
            }
        }
        function init() {
            //check_login();
            // check user as this is loaded in all pages
//        if (utilService.check_user_login() == null){
//            $location.path("/login");
//        }
            var cat_id = $stateParams.cat;

            $scope.myInterval = 3000;
            ads = adService.get_cat_level1_ads(cat_id);
            for (var i=0; i<ads.length; i++)
            {
                $scope.addSlide(ads[i].link, ads[i].href)
            }
            for (i=0; i< $scope.templates.length; i++){
                if (cat_id == $scope.templates[i].id){
                    $scope.template = $scope.templates[i];
                }
            }
        }
        init();
  });
