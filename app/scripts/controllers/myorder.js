'use strict';

/**
 * @ngdoc function
 * @name bullsstorefront1App.controller:MyorderCtrl
 * @description
 * # MyorderCtrl
 * Controller of the bullsstorefront1App
 */
angular.module('exidelife.web')
  .controller('MyorderCtrl', function ($scope, $http, $q, $location,$stateParams ,myorderService, localStorageService) {


       // var product_id = $stateParams.product_id;
        //$scope.product_id = product_id;

        init();

        function init() {

            get_order_details();

//

        }

        function get_order_details() {
            var e = localStorageService.get('user_info');
            var u;
            if (e.hasOwnProperty('email_id')) {
                u = e.email_id;
            }
            else {
                u = e.user_id;
            }
            myorderService.get_order_details($http, $q, u).then(function (data) {
                    $scope.my_orders = data;
                    console.log(data);
                },
                function () {
                    //Display an error message
                    $scope.error = error;
                    $scope.loading = false;
                });


        }


    });
