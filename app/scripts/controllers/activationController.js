/**
 * Created by Hassan Ahamed on 2/11/2015.
 */

'use strict';

angular.module('exidelife.web')
 .controller('ActivationController', function ($scope, $stateParams, $location, $http, $q, activationService,$state) {

    $scope.registration_active = {};
    $scope.registration_active.email_id = $stateParams.id;
    $scope.registration_active.guid = $stateParams.cid;


    activationService.activation($http, $q, $scope.registration_active).then(function(data){
            //Update UI using data or use the data to call another service
            alert ("Your account successfully activated. Please login now");
            $state.go("login.login", {});
        },
        function(){
            //Display an error message
            $scope.error= error;
        });
});
