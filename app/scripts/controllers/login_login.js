'use strict';

angular.module('exidelife.web')
  .controller('login_loginCtrl', function (
                            $scope,$state, $http, $q, $window, $location, $rootScope,
                            loginService,  localStorageService, cartService, utilService
                                    )
    {
        $scope.show_loader=false;
        $scope.$on("login_success", function () {
            // This function listens to any change in scope for broadcast message login_success.
            // If it gets that, applies subsequent code
//            set_login_text();




        });
        $scope.TrapKeyEvent=function(keyCode){

            console.log(keyCode);

            if(keyCode=='13'){
                $scope.validateUserService();
            }
        }
//        var set_login_text = function () {
//            //        console.log(localStorageService.get('user_info').first_name == null )   ;
//            if ((localStorageService.get('user_info') != null)) {
//                if (localStorageService.get('user_info').first_name != null) {
//                    var user_info = localStorageService.get('user_info');
//                    $scope.signin_text = ("welcome " + user_info.first_name + " " + user_info.last_name).toTitleCase();
//                    $scope.signout_text = "Sign Out";
//                }
//                else {
//                    UtilService.change_state($state, "login.login", {});
////                    $state.go("login.login");// root_url + "/login/login.html#/login";
//                    //$window.location.href = login_url;
//                }
//
//            }
//            else {
//                            console.log("open login");
//                UtilService.change_state($state, "login.login", {});
////                $window.location.href = login_url;
//            }
//        }




        function init() {
            $scope.userlogin = {"email_id": "", "password": "", "logindate": new Date(), "company": store }
          // $scope.userlogin.company=store;
            //console.log("crappy");
            //check_login();
//            var user_info;
//            set_login_text();
        }
        init();
        $scope.opts = {
            backdropFade: true,
            dialogFade: true
        };

        $scope.openRegistration = function () {
            localStorageService.set('user_info', "");
            localStorageService.set('local_cart', "[]");
            localStorageService.set('local_wishlist', "[]");

//            state_managerService.refer_url = $location.path();
            var url = root_url + "/login/login.html#/registration";
            console.log(url);
            $window.location.href = url;
        }
        $scope.openLogin = function () {

            localStorageService.set('user_info', "[]");
            localStorageService.set('local_cart', "");
            localStorageService.set('local_wishlist', "");

//            state_managerService.refer_url = $location.path();
//            var login_url = root_url + "/login/login.html#/login";
//            $window.location.href = login_url;

              $state.go('login.login',{});
        }

        $scope.execLogout = function () {
            var url;
            $window.location.href = login_url;
            if (localStorageService.get('user_info') != null) {
                localStorageService.add('user_info', "");
                this.openLogin();
            }
            else {
                localStorageService.add('user_info', "");
                this.openRegistration();
                //            url =  root_url + "/index.html#/registration" ;
                //            $window.location.href = url;

            }
        }

        $scope.validateUserService = function () {

            //console.log($scope.userlogin);

             if($scope.userlogin =="" || typeof $scope.userlogin ==="undefined"||  $scope.userlogin == null)
             {

                 alert("Please Enter UserID and Password");

                 return false;
             }

           else if($scope.userlogin.email_id =="" || typeof $scope.userlogin.email_id ==="undefined"||  $scope.userlogin.email_id == null)
            {

                alert("Please Enter UserID and Password");
                return false;
            }

           else if($scope.userlogin.password =="" || typeof $scope.userlogin.password ==="undefined"||  $scope.userlogin.password == null)
            {

                alert("Please Enter UserID and Password");
                return false;
            }


            $scope.show_loader=true;
            //console.log(state_managerService.refer_url);
            var returnval = '';
            loginService.IsValidUser($http, $q, $scope.userlogin).then(function (data) {
                    //Update UI using data or use the data to call another service
                    //$scope.message = data;
                  //  alert(data[0].message);
                    console.log(data);
                    if (data[0].message == "Log In Successfully") {

                        // now get latest cart
                        loginService.get_bulk_cart_by_email($http, $q, localStorageService.get('user_info').email_id).then(function (data) {
                                //Update UI using data or use the data to call another service


                                var string_bulk_cart = JSON.parse(data);
                                if(string_bulk_cart!="" && string_bulk_cart!=null)
                                {
                                    var bulk_cart_items = JSON.parse(string_bulk_cart);
                                    cartService.cart = bulk_cart_items;
                                    //get_wishlist_by_email();
                                    localStorageService.add('local_cart', bulk_cart_items);
                                }
                                else
                                {
                                    cartService.cart = [];
                                    //get_wishlist_by_email();
                                    localStorageService.add('local_cart', "[]");
                                }


                                console.log(cartService.cart);
                                console.log(localStorageService.get('local_cart'));

                                $rootScope.$broadcast('cart_loaded');

                                $scope.message = "Login Success";
                                $scope.user_name = $scope.userlogin.email_id;
                                $rootScope.$broadcast('login_success');
                                utilService.change_state($state, "main.home", {})
//                                if (state_managerService.refer_url == null) {
//                                    //state_managerService.refer_url = root_url + "/index.html#/home";
//                                    state_managerService.refer_url = root_url + "/index.html#/cat/2.7.1";
//                                }
//                                state_managerService.refer_url = root_url + "/index.html#/cat/2.7.1";
//                                $location.path(state_managerService.refer_url);
                                $scope.isVisible = true;

                            },
                            function () {
                                //Display an error message
                                $scope.show_loader=false;
                                $scope.error = error;
                            });


                    }
                    else {
                        $scope.show_loader=false;
                        //$scope.message = data.value;
                        alert(data[0].message);
                        $scope.message = c;
                        $scope.signin_text = "Sign In"
                        localStorageService.set('user_info', "");
                    }

                    localStorageService.set('user_info', data[0]);

                },
                function () {
                    //Display an error message
                    $scope.error = error;
                });


        };

    });
