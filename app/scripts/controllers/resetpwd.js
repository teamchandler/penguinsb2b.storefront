'use strict';

angular.module('exidelife.web')
    .controller('ResetpwdCtrl', function ($scope, $state,  $location,$stateParams, $http, $q, localStorageService, resetpwdService) {

        $scope.resetpwd = {};
        $scope.resetpwd.email_id = $stateParams.id;
        $scope.resetpwd.guid = $stateParams.cid;


        $scope.changePassword = function () {



        //    if($scope.registration.gender == '' || typeof $scope.registration.gender=== 'undefined' )
            if($scope.registration == '' || typeof $scope.registration === 'undefined')
            {
                alert('Please Enter Password');
                return false;
            }

           else if($scope.registration.password == '' || typeof $scope.registration.password === 'undefined')
            {
                alert('Please Enter Password');
                return false;
            }
           else if($scope.registration.confrmpwd == '' || typeof $scope.registration.confrmpwd === 'undefined')
            {
                alert('Please Enter Confirm Password');
                return false;
            }

           else if ($scope.registration.password !=  $scope.registration.confrmpwd )
            {
                alert('Password and Confirm Password needs to be same');
                return false;
            }


            if ($scope.registration.password == $scope.registration.confrmpwd) {
                $scope.registration.email_id = $scope.resetpwd.email_id;
                $scope.isViewLoading = true;
                var returnval = '';
                //returnval = registrationService.UserRegistration($http, $q, $scope.registration);
                $scope.registration.company = store;
                resetpwdService.SaveNewPassword($http, $q, $scope.registration).then(function (data) {

                        localStorageService.set('message', JSON.parse(data.toString()));
                        $scope.isViewLoading = false;
                        $location.path("/message");
                        //}
                    },
                    function () {
                        //Display an error message
                        $scope.error = error;
                    });
            }
            else {

                localStorageService.set('message', 'Both password should be same. ');
                $location.path("/message");
            }

        };
        // alert($scope.registration_active);
        resetpwdService.resetPwdCheck($http, $q, $scope.resetpwd).then(function (data) {
                //Update UI using data or use the data to call another service
                //alert("Your account successfully activated. Please login now");
                //alert("Your account successfully activated. Please login now");
                //$location.path("/login");
                if(data.toString()=="You Can Not Reset Your Password With This Link"){
                   // $location.path("/message");
                    $state.go('login.message');
                }
            },
            function(){
                //Display an error message
                $scope.error= error;
            });
    });
