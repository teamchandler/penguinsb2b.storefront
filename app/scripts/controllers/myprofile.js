/**
 * Created by DRIPL_1 on 08/06/2014.
 */

/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 17/1/14
 * Time: 10:03 AM
 * To change this template use File | Settings | File Templates.
 */

angular.module('exidelife.web')
.controller('myprofileCtrl', function ($scope, $http, $q,
                                                $location, my_profile,
                                                localStorageService, utilService) {

    init();

    function init() {
     //   if (utilService.get_target_management() == 0) {
       //     $scope.target_management_indicator = false;
       // }
       // else {
         //   $scope.target_management_indicator = true;
            // get emptree
            get_my_profile();
         //   get_my_work_info();
          //  get_my_scorecard();
           // $scope.day_list = my_profileService.get_day_list();
           // $scope.month_list = my_profileService.get_month_list();
           // $scope.year_list = my_profileService.get_year_list();
           // $scope.state_list = my_profileService.get_state_list();
       // }


    }

    function get_my_profile() {
        my_profile.get_my_profile($http, $q, localStorageService.get('user_info').email_id).then(function (data) {
                $scope.my_profile = data;
                $scope.user_shipping_list = data;
                //console.log($scope.monthly_tracker)
            },
            function () {
                //Display an error message
                $scope.error = error;
                $scope.loading = false;
            });
    }
//    function get_my_work_info() {
//        my_profileService.get_work_info($http, $q, localStorageService.get('user_info').email_id).then(function (data) {
//                $scope.work_info = data;
//                //console.log($scope.monthly_tracker)
//            },
//            function () {
//                //Display an error message
//                $scope.error = error;
//                $scope.loading = false;
//            });
//    }

//    function get_my_scorecard() {
//        my_profileService.get_work_scorecard_info($http, $q, utilService.get_store(), localStorageService.get('user_info').email_id).then(function (data) {
//                $scope.achievements = data[0];
//                //console.log($scope.monthly_tracker)
//            },
//            function () {
//                //Display an error message
//                $scope.error = error;
//                $scope.loading = false;
//            });
//    }


    $scope.select_address = function (selected_address) {
        populate_selected_address(selected_address);
    }

    var selected_address = {};

    var populate_selected_address = function (address) {

        selected_address.first_name = address.first_name;
        selected_address.last_name = address.last_name;
        selected_address.street = address.street;
        selected_address.address = address.address;
        selected_address.city = address.city;
        selected_address.state = address.state;
        selected_address.pincode = address.pincode;
        selected_address.mobile_no = address.mobile_no;
        selected_address.zipcode = address.zipcode;
        selected_address.email_id = localStorageService.get('user_info').email_id;
        selected_address.store = utilService.get_store().toLowerCase();
        $scope.selected_address = selected_address;
    }

    $scope.SaveUserRecord = function () {
        my_profileService.UpdateRegistrationInfo($http, $q, $scope.selected_address).then(function (data) {
                get_my_profile();
                alert(JSON.parse(data.toString()));

            },
            function () {
                //Display an error message
                $scope.error = error;
            });

    }

//    my_profileService.get_work_info($http, $q, localStorageService.get('user_info').email_id).then(function (data) {
//            $scope.work_info = data;
//            //console.log($scope.monthly_tracker)
//        },
//        function () {
//            //Display an error message
//            $scope.error = error;
//            $scope.loading = false;
//        });


});
