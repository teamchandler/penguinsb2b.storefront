
'use strict';

angular.module('exidelife.web')
    .controller('BrandDetailsController', function ($scope, $http, $location, $q,BrandService,utilService,$stateParams,$state,Cat_allService,catService) {

        init();


        $scope.shipping_days = [ "3-5 Days" , "5-7 Days", "7-10 Days", "10-15 Days", "15-20 Days"  ];

        function init() {
            //  alert('Hi');
            // check user as this is loaded in all pages
            //check_login();
            //user_info = localStorageService.get('user_info');

              var brand_name = $stateParams.name;

            //var brand_name = 'puma';

            //var lastChar = cat_id[cat_id.length - 1]; // => "1"
            ////console.log (cat_id.split(".").length)     ;
            //if ((lastChar == "0") && (cat_id.split(".").length == 2)) { // top level menu
            //    $location.path("/cat1/" + cat_id);
            //}
            //$scope.cat_id = cat_id;
            $scope.sortorder = 'Name';
            //set_filter(cat_id);
            var prod_list;


            BrandService.get_product_by_brand($http, $q, brand_name).then(function (data) {
                    //Update UI using data or use the data to call another service
                    prod_list = data;
                    //console.log(prod_list);
                    process_price_logic(prod_list);
                    get_brands();
                    get_shipping_days();
                    //get_brands(prod_list);
                    //console.log($scope.brands_filter)  ;
                },
                function () {
                    //Display an error message
                    $scope.error = error;
                });
            // search brands within searched prod list


            // for treeview

            get_all_cat_by_brand(brand_name);



        }

//        var process_price_logic = function (prod_list) {
//            var productList = [];
//            var conv_ratio = utilService.get_conversion_ratio();
//            var store_type = utilService.get_store_type();
//            var customer_discount = utilService.get_customer_discount();
//            var cat_discount = utilService.get_cat_discount();
//            var sku_discount = utilService.get_sku_discount();
//
//            var brand_discount = utilService.get_brand_discount();
//
//            var arr_cat, arr_sku, special_price;
//            special_price = 0;
//            $scope.store_type = store_type;
//
//            var filter_display = utilService.get_filter_display(); //Added By Hassan Ahamed
//            $scope.filter_display = filter_display; //Added By Hassan Ahamed Start
//
//            //if (store_type !=  "R") // store is not Retail Only
//            //{
//            for ( var i = 0; i < prod_list.length; i++) {
//                //console.log(prod_list[i]) ;
//                var  prod = JSON.parse(prod_list[i].toString());
//                if ((typeof prod.price) == "object") {
//                    var    prod_price = prod.price;
//                }
//                else {
//                    prod_price = JSON.parse(prod.price.toString());
//                }
//
//                if (customer_discount != "") {
//                    prod_price.final_offer = prod_price.final_offer * (1 - parseFloat(customer_discount).toFixed(2));
//                    special_price = 1;
//                    //console.log(prod.price.final_offer);
//                }
//                arr_cat = prod.cat_id; // cats this product belongs to
//                //for (k = 0; k < cat_discount.length; k++) // for each of deal cats
//                //{
//                //    if (arr_cat.indexOf(cat_discount[k].id) > -1) {
//                //        prod_price.final_offer = prod_price.final_offer * (1 - parseFloat(cat_discount[k].extra_off).toFixed(2));
//                //        special_price = 1;
//                //    }
//                //}
//                //for (k = 0; k < sku_discount.length; k++) // for each of deal cats
//                //{
//                //    if (prod.sku == sku_discount[k].id) {
//                //        prod_price.final_offer = prod.price_final_offer * (1 - parseFloat(sku_discount[k].extra_off).toFixed(0));
//                //        special_price = 1;
//                //    }
//                //}
//
//
//                for (var k = 0; k < cat_discount.length; k++) // for each of deal cats
//                {
//                    if (arr_cat.indexOf(cat_discount[k].id) > -1) {
//                        prod_price.final_offer = prod_price.final_offer * (1 - cat_discount[k].extra_off);
//                        special_price = 1;
//                    }
//                }
//                /* for (k = 0; k < sku_discount.length; k++) // for each of deal cats
//                 {
//                 if (prod.sku == sku_discount[k].id) {
//                 prod_price.final_offer = prod.price_final_offer * (1 - parseFloat(sku_discount[k].extra_off).toFixed(3));
//                 special_price = 1;
//                 }
//                 }*/
//                for (var k = 0; k < sku_discount.length; k++) // for each of deal cats
//                {
//                    if (prod.sku == sku_discount[k].id) {
//                        prod_price.final_offer = prod_price.final_offer * (1 - sku_discount[k].extra_off);
//                        special_price = 1;
//                    }
//                }
//
//
//                prod_price.final_offer = parseInt(prod_price.final_offer.toFixed(0));
//
//
//                /**************Added By To Calculate Special Discount For Brand***********/
//                for (var k = 0; k < brand_discount.length; k++) // for each of deal cats
//                {
//                    if (prod.brand == brand_discount[k].brand) {
//                        prod_price.final_offer = prod_price.final_offer * (1 - brand_discount[k].extra_off);
//                        special_price = 1;
//                    }
//                }
//
//                prod_price.final_offer = parseInt(prod_price.final_offer.toFixed(0));
//
//                /**********************************************************/
//
//
//
//                prod_price.final_offer = parseInt(prod_price.final_offer.toFixed(0));
//                prod_price.final_discount = (1 - (prod_price.final_offer / prod_price.mrp)) * 100;
//                // prod_price.final_discount = parseInt(prod_price.final_discount.toFixed(0));
//                prod_price.final_discount = prod_price.final_discount.toFixed(2);//Diwakar
//                prod.point = prod_price.final_offer * conv_ratio;
//                prod.point = prod.point.toFixed(0);
//
//
//
//
//
//                prod.price = prod_price;
//                //productList.push(prod);
//                var excluded = false;
//                //console.log(prod.brand.toLowerCase())   ;
//                if ((prod.stock < 0) && (prod.stock != -9999)) {
//                    excluded = true;
//                }
//                if (brand_exclusion.length > 0) {
//                    for (var j = 0; j < brand_exclusion.length; j++) {
//                        if (prod.brand.toLowerCase() === brand_exclusion[j].toLowerCase()) {
//                            console.log(prod.brand.toLowerCase());
//                            excluded = true;
//                            break;
//                        }
//                    }
//                }
//                if (excluded === false) {
//                    productList.push(prod);
//                }
//            }
//            //}
//            //alert (productList[0].price.list);
//
//            if (filter_display == "P") {  //Added By Hassan Ahamed Start
//
//                if (productList.length > 0) {
//                    $scope.productList = productList;
//                    $scope.productList_original = productList;
//                    $scope.special_price = special_price;
//                    //console.log(productList);
//                    $scope.max_price_sku = _.max($scope.productList_original,
//                        function (p) {
//                            return parseInt(p.point);
//                        }).point;
//                    $scope.min_price_sku = _.min($scope.productList_original,
//                        function (p) {
//                            return parseInt(p.point);
//                        }).point;
//                }
//            }
//
//            else {//Added By Hassan Ahamed End
//
//                if (productList.length > 0) {
//                    $scope.productList = productList;
//                    $scope.productList_original = productList;
//                    $scope.special_price = special_price;
//                    console.log(productList);
//
////                    $scope.max_price_sku = _.max($scope.productList_original,
////                        function (p) {
////                            return parseInt(p.price.final_offer);
////                        }).price.final_offer;
////                    $scope.min_price_sku =
////                        _.min($scope.productList_original,
////                            function (p) {
////                                return parseInt(p.price.final_offer);
////                            }).price.final_offer;
//                }
//            }
//        }


        var process_price_logic = function (prod_list) {
            var productList = [];
            var conv_ratio = utilService.get_conversion_ratio();
            var store_type = utilService.get_store_type();
            var filter_display = utilService.get_filter_display(); //Added By Hassan Ahamed
            var customer_discount = utilService.get_customer_discount();
            var cat_discount = utilService.get_cat_discount();
            var sku_discount = utilService.get_sku_discount();
            var brand_discount = utilService.get_brand_discount();

            //console.log(cat_discount);
            var arr_cat, arr_sku, special_price;
            special_price = 0;

            $scope.store_type = store_type;
            $scope.filter_display = filter_display; //Added By Hassan Ahamed Start
            for (var i = 0; i < prod_list.length; i++) {
                productList.push(JSON.parse(prod_list[i].toString()));
                //console.log( productList[i].price);
                if (angular.isArray(productList[i].price)) {
                    productList[i].max_price = _.max(productList[i].price,
                        function (p) {
                            return p.final_offer;
                        }).final_offer.toFixed(0);
                    productList[i].min_price_where = _.findWhere(productList[i].price, {"max_qty": 500.0});
                    productList[i].min_price = _.pick(productList[i].min_price_where, 'final_offer', function (p) {
                        return p.final_offer;
                    }).final_offer.toFixed(0);

                    // productList[i].min_price =_.min(productList[i].price,
                    //     function (p) {
                    //         return p.final_offer;
                    //    }).final_offer.toFixed(0);
                    productList[i].mrp = productList[i].price[0].mrp.toFixed(0);
                }
                else {
                    productList[i].mrp = productList[i].price.mrp.toFixed(0);
                    productList[i].min_price = 0;
                    productList[i].max_price = 0;
                }
            }

//            utilService.set_next_list(productList);
//            utilService.nextList = productList;

            $scope.productList = productList;
            //console.log($scope.productList);
            if (filter_display == "P") {//Added By Hassan Ahamed Start

                if (productList.length > 0) {
                    $scope.productList = productList;
                    $scope.productList_original = productList;
                    $scope.special_price = special_price;
                    //console.log(productList);
//                    $scope.max_price_sku = _.max($scope.productList_original,
//                        function (p) {
//                            return parseInt(p.point);
//                        }).point;
//                    $scope.min_price_sku = _.min($scope.productList_original,
//                        function (p) {
//                            return parseInt(p.point);
//                        }).point;

                    $scope.max_price_sku = 0;
                    $scope.min_price_sku = 0;
                }
            }

            else {//Added By Hassan Ahamed End

                if (productList.length > 0) {
                    $scope.productList = productList;
                    $scope.productList_original = productList;
                    $scope.special_price = special_price;

                    $scope.max_price_sku = _.max($scope.productList_original,
                        function (p) {
                            return parseInt(p.price[0].mrp);
                        }).price[0].mrp;

                    $scope.min_price_sku = _.min($scope.productList_original,
                        function (p) {
                            return parseInt(p.price[0].mrp);
                        }).price[0].mrp;

//                    angular.forEach($scope.productList_original, function(item) {
//                        max_price_sku =_.max(item.price,
//                            function (p) {
//                                return parseInt(p.mrp);
//                            });
//                    });
//
//                    angular.forEach($scope.productList_original, function(item) {
//                        min_price_sku =_.min(item.price,
//                            function (p) {
//                                return parseInt(p.mrp);
//                            });
//                    });

                }
            }
        };


        $scope.sort_defn = "Sort By ..."
        $scope.$watch('sortorder', function () {
            switch ($scope.sortorder) {
                case "Name":
                    $scope.sort_defn = "Name";
                    break;
                case "+price.final_offer":
                    $scope.sort_defn = "Price (Low to High)";
                    break;
                case "-price.final_offer":
                    $scope.sort_defn = "Price (High to Low)";
                    break;
                case "-price.discount":
                    $scope.sort_defn = "Overall Discount (high to low)";
                    break;
                case "+brand":
                    $scope.sort_defn = "Brand";
                    break;
                default:
                    $scope.sort_defn = "Sort By ...";
                    break;
            }
        })

        var get_brands = function () {
            var brands_filter, tmp = [];
            for (var i = 0; i < $scope.productList.length; i++) {
                var excluded = false;
                var prod_brand = $scope.productList[i].brand;
                console.log(prod_brand.toLowerCase());
                for (var j = 0; j < brand_exclusion.length; j++) {
                    if (prod_brand.toLowerCase() === brand_exclusion[j].toLowerCase()) {
                        excluded = true;
                        break;
                    }
                }
                if (excluded === false) {
                    tmp.push(
                        prod_brand.toUpperCase()
                    )
                }

            }
            $scope.brand_filter = _.uniq(tmp);
        };


        var get_shipping_days = function () {
            var tmp = [];
            for (var i = 0; i < $scope.productList.length; i++) {
                var excluded = false;
                var prod_shipping_days = $scope.productList[i].shipping_days;
                if (excluded === false) {
                    tmp.push(prod_shipping_days);
                }
            }
            var shipping_days_array = [];
            var shipping_days = [];
            shipping_days_array = _.uniq(tmp);
            var min_ship_days = [];
            angular.forEach(shipping_days_array, function (item) {
                var tmp = item.split("-");
                min_ship_days.push(parseInt(tmp[0]));
            });

            for (var i = 0; i < shipping_days_array.length; i++) {
                var min_ship = 0;
                min_ship = _.min(min_ship_days, function (p) {
                    return parseInt(p);
                });
                angular.forEach(shipping_days_array, function (item) {
                    var tmp = item.split("-");
                    if (parseInt(tmp[0]) == min_ship) {
                        shipping_days.push(item);
                        var index = min_ship_days.indexOf(min_ship);
                        if (index > -1) {
                            min_ship_days.splice(index, 1);
                        }
                    }

                });

            }
            $scope.shipping_days_filter = shipping_days;
            console.log(shipping_days);
            console.log($scope.shipping_days_filter);
        };
        var filters = [];
        $scope.apply_filter = function (filter_name, filter_value) {

            var found = false;
            for (i = 0; i < filters.length; i++) {
                if (filters[i].name == filter_name) {
                    if (filters[i].value == filter_value) {
                        if ((filter_name != "discount") && (filter_name != "price")) {
                            filters.splice(i, 1);
                            found = true;
                        }
                    }
                }
            }

            if (!found) {
                filters.push({ name: filter_name, value: filter_value });
            }
            if (filter_name == "price") {
                var filter_price = true;
            }
            if (filter_name == "discount") {
                var filter_discount = true;
            }
            var brand_filter = _.where(filters, { name: "brand" });
            var shipping_days_filter = _.where(filters, { name: "shipping_days" });
            var price_filter = _.where(filters, { name: "price" });
            var discount_filter = _.where(filters, { name: "discount" });
            var feature_filter = _.difference(filters, brand_filter, price_filter, discount_filter);// .without(filters, {name:"brand"});

            //        console.log(feature_filter );

            // apply filter on original productlist and copy to display one
            var prodlist_tmp = $scope.productList_original;
            if (filters.length != 0) {
                var l_found;
                var l_brand_found;
                var l_ship_found;

                var prodList_filtered = [];
                var prodList_brand_filtered = [];
                var prodList_ship_filtered = [];
                var prod_list_feature_filtered = [];
                var prod_list_price_filtered = [];
                var prod_list_discount_filtered = [];

                // first filter for features

                if (brand_filter.length > 0) {
                    for (var i = 0; i < prodlist_tmp.length; i++) {
                        l_brand_found = false;
                        var prod_tmp = prodlist_tmp[i];
                        for (var p = 0; p < brand_filter.length; p++) {
                            if (prod_tmp.brand.toLowerCase() == brand_filter[p].value.toLowerCase()) {
                                l_brand_found = true;
                                break;
                            }
                        }
                        if (l_brand_found) {
                            prodList_brand_filtered.push(prod_tmp);
                        }

                    }
                }
                else {
                    prodList_brand_filtered = prodlist_tmp;
                }
                //console.log(prodList_filtered);

                // now filter for Shipping Days apply that on prodList_filtered
                if (shipping_days_filter.length > 0) {
                    for (var i = 0; i < prodList_brand_filtered.length; i++) {
                        l_ship_found = false;
                        var prod_tmp = prodlist_tmp[i];
                        for (var p = 0; p < shipping_days_filter.length; p++) {
                            if (prod_tmp.shipping_days == shipping_days_filter[p].value) {
                                l_ship_found = true;
                                break;
                            }
                        }
                        if (l_ship_found) {
                            prodList_filtered.push(prod_tmp);
                        }

                    }
                }
                else {
                    prodList_filtered = prodList_brand_filtered;
                }

                // now filter for pricerange apply that on prodList_filtered
                if (filter_price) {

                    for (var i = 0; i < prodList_filtered.length; i++) {

                        var prod_tmp = prodList_filtered[i];

                        //console.log(prod_tmp);

                        if (filter_display == "P") { //Added By Hassan Ahamed Start

//                            if (
//                                (parseInt(prod_tmp.point) >= $scope.min_price_sku)
//                                &&
//                                (parseInt(prod_tmp.point) <= $scope.max_price_sku)
//                                ) {
//                                // push to new prodlist
//                                prod_list_price_filtered.push(prod_tmp);
//                            }

                        }
                        else {//Added By Hassan Ahamed End

                            if (
                                (parseInt(prod_tmp.price[0].mrp) >= $scope.min_price_sku)
                                &&
                                (parseInt(prod_tmp.price[0].mrp) <= $scope.max_price_sku)
                                ) {
                                // push to new prodlist
                                prod_list_price_filtered.push(prod_tmp);
                            }
                        }


                    }
                    prodList_filtered = prod_list_price_filtered;
                }
                //console.log(prodList_filtered);
                // now filter for discount apply that on prodList_filtered




            // *********************
            $scope.productList = prodList_filtered;

        }
        else
        {
            $scope.productList = $scope.productList_original;
        }


     }


//        catService.get_breadcrumb($http, $q, cat_id).then(function (data) {
//                //Update UI using data or use the data to call another service
//                var id, level;
//                var cats = data;
//                $scope.cats = [];
//                // hardcoded home node
//                var home = '{"id": "0","Name": "Home","description": "Go to Home", "level": 0}';
//                $scope.cats.push(JSON.parse(home));
//                for ( i = 0; i < cats.length; i++) {
//                    $scope.cats.push(JSON.parse(cats[i].toString()));
//                }
//                for (var i = 0; i < $scope.cats.length; i++) {
//                    id = $scope.cats[i].id;
//                    var tmp = id.split(".");
//                    if (tmp.length <= 2) {
//                        // level 1
//                        level = 1
//                    }
//                    else {
//                        if (tmp[2] == "0") {
//                            level = 2;
//                        }
//                        else {
//                            level = 3;
//                        }
//                    }
//                    $scope.cats[i].level = level;
//                }
//                //                console.log($scope.cats);
//            },
//            function () {
//                //Display an error message
//                $scope.error = error;
//            });



        function get_all_cat_by_brand(brand)
        {
            $scope.brand_name = brand;
            Cat_allService.get_cat_by_brand($http, $q , $scope.brand_name).then(function (data){

                    $scope.cat_details = data[0];

//               for(var i=0;i< $scope.cat_details.brand_cat_associate.length;i++)
//               {
//
//                   var level = $scope.cat_details.brand_cat_associate[i].catlevel1_name;
//                   if(level == "Men")
//                   {
//                    $scope.Level1 = level;
//                    $scope.level1_id = $scope.cat_details.brand_cat_associate[i].catlevel_1;
//
//                    var level_2 = $scope.cat_details.brand_cat_associate[i].catlevel2_name;
//                       if(level_2 == "Apparel")
//                       {
//                           $scope.Level2_cat = level_2;
//                           $scope.level2_id_cat = $scope.cat_details.brand_cat_associate[i].catlevel_2;
//                           $scope.level3_cat = $scope.cat_details.brand_cat_associate[i].catlevel3_name;
//                           $scope.level3_cat_id = $scope.cat_details.brand_cat_associate[i].catlevel_3;
//
//
//
//                           $scope.cat_level_3_men = [];
//                           var cat_level_3 = {
//                               level_3: $scope.level3_cat,
//                               level_3_id:$scope.level3_cat_id
//
//                           };
//
//                           $scope.cat_level_3_men.push(cat_level_3);
//
//
//                       }
//
//
//                   }
//                   if(level == "Women")
//                   {
//                       $scope.Level2 = level;
//                       $scope.level2_id = $scope.cat_details.brand_cat_associate[i].catlevel_2;
//
//                       var women_level_2 = $scope.cat_details.brand_cat_associate[i].catlevel2_name;
//                       if(women_level_2 == "Apparel")
//                       {
//                           $scope.Level2_cat_woman = women_level_2;
//                           $scope.level2_id_cat_woman = $scope.cat_details.brand_cat_associate[i].catlevel_2;
//                       }
//
//
//                   }
//
//
//               }

                },

                function () {
                    //Display an error message
                    $scope.error = error;

                });


        }


    });

