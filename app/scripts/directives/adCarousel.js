/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 13/7/13
 * Time: 7:15 AM
 * To change this template use File | Settings | File Templates.
 */
angular.module('exidelife.web')
    .directive('adCarousel', function ($timeout) {
    return {
        templateUrl: 'views/adCarousel.html',
        restrict: 'EAC',
        // responsible for registering DOM listeners as well as updating the DOM
        link: function (scope,element,attr) {
            scope.$watch('ad', function () {
                scope.pics = scope.$eval(attr.pics);
                $(element).carousel({
                    interval: 0
                });
            });
        }
    }
});


