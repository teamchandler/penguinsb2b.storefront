/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 29/6/13
 * Time: 8:16 AM
 * To change this template use File | Settings | File Templates.
 */

//var App = angular.module('zoom', []);
angular.module('exidelife.web')
    .directive('zoomView', function ($timeout) {
    return {
    // Restrict it to be an attribute in this case
    restrict: 'A',
    // responsible for registering DOM listeners as well as updating the DOM
    link: function(scope, element, attrs) {
            attrs.$observe('zoomView', function(v) {
                $(element).zoom({url: scope.$eval(v).url});//"http://cdn-new-annectos.s3.amazonaws.com/images/sarees/z/banarasbds_zoom.jpg"})
            })

    }
    }
});
