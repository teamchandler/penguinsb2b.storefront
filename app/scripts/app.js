'use strict';

angular.module('exidelife.web', []);


'use strict';

angular.module('exidelife.web', [
//    'ngAnimate',
//    'ngRoute',
    'LocalStorageModule',
    'ui.router',
    'ui.bootstrap'
])


    .config( function ($stateProvider, $urlRouterProvider) {



        $urlRouterProvider.otherwise('/main/home');

        $stateProvider
            .state('login', {
                url: "/login",
                abstract: true,
                templateUrl: "views/login_base.html",
                controller : "LoginBaseCtrl"
            })
            .state('login.login', {
                    url: '/login',
                    templateUrl: 'views/login_login.html',
                    data: {
                        "header_label" : "Please Sign In"
                    },
                    controller: 'login_loginCtrl'
            })
            // the pet tab has its own child nav-view and history
//            .state('login.forget_password', {
//                url: '/forget_password',
//                        templateUrl: 'views/forget_password.html',
//                        controller: 'ForgetPasswordCtrl',
//                        data: {
//                            "header_label" : "Forget Password"
//                        }
//            })

            .state('login.reset_password', {
                url: '/reset_password/:id/:cid',
                templateUrl: 'views/resetpwd.html',
                controller: 'ResetpwdCtrl',
                data: {
                    "header_label" : "Reset Password"
                }
            })

            .state('login.forget_password', {
                url: '/forget_password',
                templateUrl: 'views/forget_password.html',
                controller: 'forget_pwd_Controller',
                data: {
                    "header_label" : "Forget Password"
                }
            })

            .state('login.message', {
            url: '/message',
            templateUrl: 'views/message.html',
            controller: 'MessageCtrl',
            data: {
                "header_label" : "Reset Password"
            }
            })

            .state('login.registration', {
                url: '/registration',
                        templateUrl: 'views/registration.html',
                        controller: 'registrationController',
                        data: {
                            "header_label" : "Register"
                        }
            })
            .state('login.activation', {
                url: '/activation/:id/:cid',
                templateUrl: 'views/activation.html',
                controller: 'ActivationController',
                data: {
                    "header_label" : "Account Activation"
                }
            })

            .state('login.support', {
                url: '/support',
                templateUrl: 'views/support.html',
                controller: 'support_controller',
                data: {
                    "header_label" : "Support Numbers"
                }
            })


            .state('main', {
                url: "/main",
                abstract: true,
                templateUrl: "views/main_base.html",
                controller : "MainBaseCtrl"
            })
            .state('main.home', {
                url: '/home',
                templateUrl: 'views/home.html',
                controller: 'HomeCtrl',
                data: {
                    "header_label" : "Dashboard"
                }
            })
            .state('main.cart', {
                url: '/cart',
                templateUrl: 'views/cart.html',
                controller: 'CartCtrl',
                data: {
                    "header_label" : "Cart"
                }
            })
            .state('main.brand', {
                url: '/brand',
                templateUrl: 'views/brand.html',
                controller: 'BrandController',
                data: {
                    "header_label" : "Brand"
                }
            })

            .state('main.brand_details', {
                url: '/brand_details/:name',
                templateUrl: 'views/brand_details.html',
               controller: 'BrandDetailsController',
                data: {
                    "header_label" : "Brand"
                }
            })
            .state('main.cat_details', {
                url: '/cat_details',
                templateUrl: 'views/all_cat.html',
                controller: 'cat_allController',
                data: {
                    "header_label" : "Brand"
                }
            })
            .state('main.cat', {
                url: '/cat/:catid',
                templateUrl: 'views/cat.html',
                controller: 'CatCtrl',
                data: {
                    "header_label" : "Product List"
                }
            })

            .state('main.special_cat', {
                url: '/special_cat/:menu_type/:price_range/:parent_cat_id/:cat_name',
                templateUrl: 'views/special_cat.html',
                controller: 'SpecialCatCtrl',
                data: {
                    "header_label" : "Product List"
                }
            })

            .state('main.search', {
                url: '/search/:srch/:context',
                templateUrl: 'views/search.html',
                controller: 'SearchCtrl',
                data: {
                    "header_label" : "Search Result"
                }
            })
            .state('main.cat1', {
                url: '/cat1/:catid',
                templateUrl: 'views/cat1.html',
                controller: 'Cat1Ctrl',
                data: {
                    "header_label" : "Product List"
                }
            })
            .state('main.prod', {
                url: '/prod/:prodid',
                templateUrl: 'views/prod.html',
                controller: 'ProdCtrl',
                data: {
                    "header_label" : "Product List"
                }
            })
            .state('main.enq', {
                url: '/enq/:product_id',
                templateUrl: 'views/enq.html',
                controller: 'enqCtrl',
                data: {
                    "header_label" : "Enquire Now Details"
                }
            })
            .state('main.ship', {
                url: '/ship',
                templateUrl: 'views/ship.html',
                controller: 'shipCtrl',
                data: {
                    "header_label" : "Shipment Details"
                }
            })
            .state('main.payment', {
                url: '/payment/:order_id',
                templateUrl: 'views/pp.html',
                controller: 'ppController',
                data: {
                    "header_label" : "Payment Details"
                }
            })
		    .state('main.checkout', {
                url: '/checkout/:order_id',
                templateUrl: 'views/cc.html',
                controller: 'checkCtrl',
                data: {
                    "header_label" : "Checkout "
                }
            })
	        .state('main.payment_details', {
                url:'/payment_details/:order_id',
                templateUrl: 'views/cp.html',
                controller: 'check_popupController',
                data: {
                        "header_label" : "Payment Details "
                    }

            })
            .state('main.bank_details',{
            url:'/bank_details/:order_id',
            templateUrl: 'views/bank_details.html',
            controller: 'bank_popupController',
            data: {
                "header_label" : "Bank Details "
            }

        })
            .state('main.contactus', {
            url: '/contact_us',
            templateUrl: 'views/contactus.html',
            controller: 'contactusCtrl',
            data: {
                "header_label" : "Contact Us Details"
            }
        })
            .state('main.myorder', {
                url: '/myorder',
                templateUrl: 'views/myorder.html',
                controller: 'MyorderCtrl',
                data: {
                    "header_label" : "My Order"
                }
            })

            .state('main.myprofile', {
                url: '/myprofile',
                templateUrl: 'views/myprofile.html',
                controller: 'myprofileCtrl',
                data: {
                    "header_label" : "My Profile"
                }
            })

            .state('main.myenquiry', {
                url: '/myenquiry',
                templateUrl: 'views/myenquiry.html',
                controller: 'myenquiryCtrl',
                data: {
                    "header_label" : "My Enquiry"
                }
            })


            .state('main.changepassword', {
                url: '/changepassword',
                templateUrl: 'views/change_password.html',
                controller: 'change_password',
                data: {
                    "header_label" : "Change Password"
                }
            })



            /***********For Payment*************/

            .state('base_payment', {
                url: "/base_payment",
                abstract: true,
                templateUrl: "views/payment_base.html",
                controller : "PaymentBaseCtrl"
            })
            .state('base_payment.payment', {
                url: '/payment/:status',
                templateUrl: 'views/payment.html',
                controller: 'PaymentCtrl',
                data: {
                    "header_label" : "Payment"
                }
            })
            .state('main.privacy_policy', {
                url: '/privacy_policy',
                templateUrl: 'views/privacy_policy.html',
                //controller: 'myenquiryCtrl',//
                data: {
                    "header_label" : "Privacy policy"
                }
            })
            .state('main.security_policy', {
                url: '/security_policy',
                templateUrl: 'views/security_policy.html',
                //controller: 'myenquiryCtrl',//
                data: {
                    "header_label" : "Security Policy"
                }
            })
            .state('main.terms', {
                url: '/terms',
                templateUrl: 'views/terms.html',
                //controller: 'termsCtrl',//
                data: {
                    "header_label" : "Terms"
                }
            })



        ;

    });
