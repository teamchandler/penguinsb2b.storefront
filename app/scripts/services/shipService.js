/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 6/10/13
 * Time: 9:01 PM
 * To change this template use File | Settings | File Templates.
 */

angular.module('exidelife.web')

    .service('shipService', function shipService($rootScope) {

    this.get_state_list = function(){
        return state_list;
    }

    this.get_country_list = function(){
        return country_list;
    }

    this.get_user_shipping_list = function($http, $q, user_id){
        var apiPath = cat_service_url + '/store/user_shipping?email=' + user_id ;
        var deferred = $q.defer();

        $http({
            method: 'GET',
            url: apiPath,
            type: JSON
        }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("An error occured while validating User");
            })
        return deferred.promise;

    }
    this.save_shipping_page = function($http, $q, dataobj){
        var apiPath = cat_service_url + '/store/checkout/write_bulk_ship_data/';
        var deferred = $q.defer();

        $http({
            method: 'POST',
            url: apiPath,
            data: dataobj,
            type: JSON
        }).success(function (data) {
                deferred.resolve(data);
                //localStorageService.add('user_info', data[0]);
            }).error(function (data)
            {
                deferred.reject("An error occured while validating User");
            })
        return deferred.promise;

    }


        this.get_profile_info = function($http, $q, email_id ){
//            var apiPath = cat_service_url +
//                '/userinfo/by/user/'+ email_id ;

            var apiPath = cat_service_url +'/tracker/m_profile?emp_id=' + email_id + '&json=true';

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }



//Stub - To be deleted *********************************
    var state_list = [
        {id: "KA", text: "Karnataka"},
        {id: "TN", text: "Tamil Nadu"},
        {id: "KL", text: "Kerala"},
        {id: "AP", text: "Andhra Pradesh"},
        {id: "OR", text: "Orissa"},
        {id: "WB", text: "West Bengal"},
        {id: "GA", text: "Goa"},
        {id: "MN", text: "Manipur"},
        {id: "Pb", text: "Punjab"},
        {id: "UP", text: "Uttar Preadesh"},
        {id: "AR", text: "Arunachal Pradesh"},
        {id: "GJ", text: "Gujarat"},
        {id: "ML", text: "Meghalaya"},
        {id: "RJ", text: "Rajasthan"},
        {id: "UT", text: "Uttaranchal"},
        {id: "AS", text: "Assam"},
        {id: "HR", text: "Haryana"},
        {id: "MZ", text: "Mizoram"},
        {id: "SK", text: "Sikkim"},
        {id: "BR", text: "Bihar"},
        {id: "HP", text: "Himachal Pradesh"},
        {id: "MP", text: "Madhya Pradesh"},
        {id: "NL", text: "Nagaland"},
        {id: "CH", text: "Chhattisgarh"},
        {id: "MH", text: "Maharashtra"},
        {id: "JK", text: "Jammu and Kashmir"},
        {id: "TR", text: " Tripura"}
];
var country_list = [
  "India"
];



});