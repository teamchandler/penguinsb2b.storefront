'use strict';

angular.module('exidelife.web')
  .service('productService', function ProductService() {
        this.get_productDetailsById = function ($http, $q, product_id) {
            var apiPath = cat_service_url +  '/store/prod_details?prod_id=' + product_id + '&json=true';

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                //data: data,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };

        this.prod_view_insert = function ($http, $q, prod) {
            var apiPath = cat_service_url + '/audit/prod_view/insert/';
            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: apiPath,
                data: prod,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);

            }).error(function (data)
            {
                deferred.reject("An error occured while validating User");
            })
            return deferred.promise;
        };

        this.get_prod_ship = function ($http, $q, cat_id) {
            var apiPath = cat_service_url + '/store/shipping_details?cat_id=' + cat_id + '&json=true';

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                //data: data,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };
  });
