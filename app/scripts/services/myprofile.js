/**
 * Created by DRIPL_1 on 08/04/2014.
 */

/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 17/1/14
 * Time: 10:05 AM
 * To change this template use File | Settings | File Templates.
 */

angular.module('exidelife.web')
.service('my_profile', function (localStorageService, utilService) {

//    this.get_day_list = function () {
//        return day_list;
//    }

//    this.get_month_list = function () {
//        return month_list;
//    }
//    this.get_year_list = function () {
//
//        var year_list = new Array();
//
//        for (i = 0; i < 201; i++) {
//            var yr = (1900 + i);
//            var year = { id: yr, text: yr };
//            year_list[i] = year;
//        }
//
//        return year_list;
//    }
//    this.get_state_list = function () {
//        return state_list;
//    }


    this.get_my_profile = function ($http, $q, user_id) {
        var apiPath = '/tracker/m_profile?emp_id=' + user_id + '&json=true';
        return get_data($http, $q, user_id, apiPath)
    }

    this.get_work_info = function ($http, $q, user_id) {
        var apiPath = '/tracker/work_info?emp_id=' + user_id + '&json=true';
        return get_data($http, $q, user_id, apiPath)
    }

    this.get_work_scorecard_info = function ($http, $q, company, user_id) {
        var apiPath = '/tracker/sales_achievement_data?company=' + company + '&emp_id=' + user_id + '&json=true/';
        return get_data($http, $q, user_id, apiPath);
    }
    this.UpdateRegistrationInfo = function ($http, $q, dataobj) {
        var apiPath = cat_service_url + '/user/registration/update_registration_info/';
        var deferred = $q.defer();

        $http({
            method: 'POST',
            url: apiPath,
            data: dataobj,
            type: JSON
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (data) {
            deferred.reject("An error occured while validating User");
        })
        return deferred.promise;
    }
        this.get_enquire_data = function ($http, $q, user_id) {
            var apiPath = cat_service_url + '/user/registration/get_product_enquire_info?email=' + user_id +'&json=true' ;
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })
            return deferred.promise;
        };

    var get_data = function ($http, $q, user_id, api) {
        var apiPath = cat_service_url + api;
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: apiPath,
            type: JSON
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (data) {
            deferred.reject("An error occured while validating User");
        })
        return deferred.promise;
    };




//    var day_list = [
//        { id: "01", text: "01" },
//        { id: "02", text: "02" },
//        { id: "03", text: "03" },
//        { id: "04", text: "04" },
//        { id: "05", text: "05" },
//        { id: "06", text: "06" },
//        { id: "07", text: "07" },
//        { id: "08", text: "08" },
//        { id: "09", text: "09" },
//        { id: "10", text: "10" },
//        { id: "11", text: "11" },
//        { id: "12", text: "12" },
//        { id: "13", text: "13" },
//        { id: "14", text: "14" },
//        { id: "15", text: "15" },
//        { id: "16", text: "16" },
//        { id: "17", text: "17" },
//        { id: "18", text: "18" },
//        { id: "19", text: "19" },
//        { id: "20", text: "20" },
//        { id: "21", text: "21" },
//        { id: "22", text: "22" },
//        { id: "23", text: "23" },
//        { id: "24", text: "24" },
//        { id: "25", text: "25" },
//        { id: "26", text: "26" },
//        { id: "27", text: "27" },
//        { id: "28", text: "28" },
//        { id: "29", text: "29" },
//        { id: "30", text: "30" },
//        { id: "31", text: "31" }
//    ];


//    var month_list = [
//        { id: "01", text: "01" },
//        { id: "02", text: "02" },
//        { id: "03", text: "03" },
//        { id: "04", text: "04" },
//        { id: "05", text: "05" },
//        { id: "06", text: "06" },
//        { id: "07", text: "07" },
//        { id: "08", text: "08" },
//        { id: "09", text: "09" },
//        { id: "10", text: "10" },
//        { id: "11", text: "11" },
//        { id: "12", text: "12" }
//
//    ];



//    var state_list = [
//        { id: "AP", text: "Andhra Pradesh" },
//        { id: "AR", text: "Arunachal Pradesh" },
//        { id: "AS", text: "Assam" },
//        { id: "BR", text: "Bihar" },
//        { id: "CH", text: "Chhattisgarh" },
//        { id: "GA", text: "Goa" },
//        { id: "GJ", text: "Gujarat" },
//        { id: "HR", text: "Haryana" },
//        { id: "HP", text: "Himachal Pradesh" },
//        { id: "JK", text: "Jammu and Kashmir" },
//        { id: "KA", text: "Karnataka" },
//        { id: "KL", text: "Kerala" },
//        { id: "MP", text: "Madhya Pradesh" },
//        { id: "MH", text: "Maharashtra" },
//        { id: "MN", text: "Manipur" },
//        { id: "ML", text: "Meghalaya" },
//        { id: "MZ", text: "Mizoram" },
//        { id: "NL", text: "Nagaland" },
//        { id: "OR", text: "Orissa" },
//        { id: "Pb", text: "Punjab" },
//        { id: "UP", text: "Uttar Preadesh" },
//        { id: "UT", text: "Uttaranchal" },
//        { id: "RJ", text: "Rajasthan" },
//        { id: "SK", text: "Sikkim" },
//        { id: "TN", text: "Tamil Nadu" },
//        { id: "TR", text: "Tripura" },
//        { id: "WB", text: "West Bengal" }];

});
