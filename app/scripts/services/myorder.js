/**
 * Created by developer6 on 6/25/2014.
 */

'use strict';

angular.module('exidelife.web')
    .service('myorderService', function CartService(localStorageService, utilService) {
// AngularJS will instantiate a singleton by calling "new" on this function


        this.get_order_details = function ($http, $q, user_id) {
            var apiPath = cat_service_url + '/store/my_order_details?email=' + user_id ;
            var deferred = $q.defer();

            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("An error occured while validating User");
            })
            return deferred.promise;

        }


        this.get_my_order_details = function ($http, $q, order_id) {
            var apiPath = cat_service_url + '/store/my_order_details?order_id=' + order_id ;
            var deferred = $q.defer();

            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("An error occured while validating User");
            })
            return deferred.promise;

        }


    });