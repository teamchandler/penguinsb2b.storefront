

angular.module('exidelife.web')
    .service('registrationService', function () {
        this.UserRegistration = function ($http, $q, dataobj) {
            var apiPath = cat_service_url + '/user/registration/submit_registration/';
            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: apiPath,
                data: dataobj,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })
            return deferred.promise;

        };

        this.ResendActCode = function ($http, $q, dataobj) {
            var apiPath = cat_service_url + '/user/registration/ResendActCode/';
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data: dataobj,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })
            return deferred.promise;

        };
    });