'use strict';

angular.module('exidelife.web')

    .service('NotificationService', function NotificationService($rootScope) {

// ***************************************************************
//          Data Elements that will be shared across
// ***************************************************************
//        var new_product_list = "";
//        var min_price = 0;
//        var max_price = 0;
// ***************************************************************

// ***************************************************************
//          This is to centralize all notifcations used
//          in all the controllers
// ***************************************************************
        // This is used for sending broadcast message and data from rasing controller
        this.product_list_changed = function (broadcast_message, product_list, min, max) {
            this.new_product_list = product_list;
            this.min_price = min;
            this.max_price = max;
            this.notify(broadcast_message)
        };

        this.cart_added = function (broadcast_message, product) {
            this.added_product = product;
            this.notify(broadcast_message)
        };

        this.cart_deleted = function (broadcast_message, product) {
            this.deleted_product = product;
            this.notify(broadcast_message)
        };

        this.cart_changed = function (broadcast_message, product) {
            this.changed_product = product;
            this.notify(broadcast_message)
        };
        this.cart_blank = function (broadcast_message) {
            this.notify(broadcast_message)
        };

        this.cal_amount = function (broadcast_message, product) {
            this.cal_amount = product;
            this.notify(broadcast_message)
        };


        this.error_occured = function (broadcast_message, error_object, source, o) {
            this.error_object  = error_object;
            this.error_source = source;
            this.o = o;
            this.notify(broadcast_message)
        };
        this.change_header = function (broadcast_message, header_label) {
            this.header_label =  header_label;
            console.log(this.header_label);
            this.notify("change header")
        };

        // This broadcast the message so that handler can pickup changed data element
        this.notify = function(broadcast_message) {
            $rootScope.$broadcast(broadcast_message);
        };

// ***************************************************************
//          End Notification
// ***************************************************************




    });
