/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 14/9/13
 * Time: 1:25 PM
 * To change this template use File | Settings | File Templates.
 */
 
 angular.module('exidelife.web')
 
.service('acctService', function (localStorageService, utilService) {

    this.get_points = function(){
        return  localStorageService.get("user_info").total_point;
    }

    this.set_points = function(point_redeemed){
        bal   =localStorageService.get("user_info").total_point-point_redeemed;
        var user =   localStorageService.get("user_info");
        user.total_point = bal;
        localStorageService.set("user_info", user);
    }
    this.get_points_credit = function($http, $q, user_id){
        var apiPath = cat_service_url + '/store/points_credit?email=' + user_id ;
        var deferred = $q.defer();

        $http({
            method: 'GET',
            url: apiPath,
            type: JSON
        }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("An error occured while validating User");
            })
        return deferred.promise;

    }
    this.get_points_debit = function($http, $q, user_id){
        var apiPath = cat_service_url + '/store/points_debit?email=' + user_id ;
        var deferred = $q.defer();

        $http({
            method: 'GET',
            url: apiPath,
            type: JSON
        }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("An error occured while validating User");
            })
        return deferred.promise;

    }
});