
'use strict';

angular.module('exidelife.web')
    .service('forgotpwdService', function () {
        this.ResetUserPassword = function ($http, $q, dataobj) {

            var apiPath = cat_service_url + '/user/registration/ResetPassword/';
            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: apiPath,
                data: dataobj,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })
            return deferred.promise;

        };


    });