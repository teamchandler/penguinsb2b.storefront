'use strict';

angular.module('exidelife.web')
  .service('cartService', function CartService(localStorageService, utilService) {
    // AngularJS will instantiate a singleton by calling "new" on this function


        var express_shipping;
        if  (localStorageService.get('express_shipping') != null)
        {
            express_shipping  =localStorageService.get('express_shipping');
        }
        else {
            express_shipping  = 0;
        }
        var cart = [];
//    return {
//        getexpress_shipping: function () {
//            return express_shipping;
//        },
//        setexpress_shipping: function(value) {
//            express_shipping = value;
//        }
//    }
        //localStorageService.add('local_cart',cartService.get_Cart());
        var load_cart_from_storage = function () {

            if ((cart.length == 1) || (cart.length ==0)) {
                if  (localStorageService.get('local_cart') != null)
                {
                    cart =localStorageService.get('local_cart');
                }
            }
        }
        this.get_cart = function () {


            load_cart_from_storage();
            return cart;
//            if ((cart.length == 1) || (cart.length ==0))
//
//            {
//                if ((cart.length ==1) && (typeof cart[0].id === 'undefined')){
//                    //console.log("inside");
//                    // for first time browser load assuming cart is stored in localstorage
//                    this.load_cart_from_storage();
//                }
//            }
//            //console.log(cart);
//            if (cart != null) {
//                if ((cart[0] != null) && (cart[0].id == null)){
//                    cart.splice(0,1);
//                }
//            }
//            return  cart;
        };

//        var ord_amt=0;

        this.get_cart_amount = function () {

            var tmp = this.get_cart();
            console.log(tmp);
            var total_amount ;
            var min_shipping = utilService.get_min_shipping();
            var shipping_charge = utilService.get_shipping_charge();
            var shipping = 0;
            var total_amount_billable;
            var total_amount_final_ord = 0;
            for (var i = 0; i < tmp.length; i++) {
               // total_amount_final_ord = total_amount_final_ord + tmp[i].parent_prod_detail.price[0].final_offer * tmp[i].order_quantity;
                total_amount_final_ord = total_amount_final_ord + (tmp[i].order_amount*1);
            }

            if  (localStorageService.get('shipping') != null)
            {
                shipping  =localStorageService.get('shipping');
            }
            else {
                shipping  = 0;
            }

//            if (total_amount_final_ord < min_shipping) {
//                shipping  = shipping_charge;
//            }
//            else{
//                shipping = 0;
//            }
              total_amount_billable = total_amount_final_ord + parseInt(shipping)  ;
//            ord_amt=total_amount_billable;
//            NotificationService.cal_amount("cal amount", total_amount_billable)
              return total_amount_billable;


        };




        this.get_cart_size = function () {

//            localStorageService.add('local_cart', "[]");

            // for bulk we are returning only number of SKUs being ordered in the size
            // which will be shown in the page
            load_cart_from_storage();
            return cart.length;
//            if ((cart.length == 1) || (cart.length == 0)) {
//                this.load_cart_from_storage();
//            }
//            var size ;
//            size = 0;
//            for (var i=1; i<cart.length; i++){
//                size = size + cart[i].quantity;
//            }
//            return  size;
        };
        this.get_Cart_Item_Size = function () {

            var cart_size=0;
            for ( var i=0; i<cart.length; i++){
                cart_size = cart_size + parseInt(cart[i].quantity);
            }
            return  cart_size;
        };
        this.update_to_new_cart = function (new_cart) {

            localStorageService.add('local_cart',new_cart);
        }
        this.update_cart_item_size = function (cart_item_id, new_sku) {

            for (var i=0; i<cart.length; i++){
                if (cart[i].cart_item_id== cart_item_id){
                    cart[i].sku =new_sku;
                    break;
                }
            }
            localStorageService.add('local_cart',cart);
        }

        this.load_cart = function (cart_item){

            cart.push({
                cart_item_id :  cart_item.sku,
                id: cart_item._id,
                sku: cart_item.sku,
                brand: cart_item.brand,
                quantity : cart_item.quantity,
                mrp: cart_item.mrp,
                final_offer:cart_item.final_offer,
                name: cart_item.Name,
                image: cart_item.image_urls[0]   ,
                discount: cart_item.discount,
                sizes: cart_item.sizes,
                express: cart_item.express
            });
            localStorageService.add('local_cart',cart);
        }


        this.addTocart = function (product, $http, $q) {

            var found;
            found = false;
            var cart_length=cart.length;
            for (var i=0; i< cart_length ; i++){
                if(cart[i].parent_prod_detail.sku==product.parent_prod_detail.sku){
                   // cart.splice(i,1);
                    cart[i]=product;
                    found = true;
                    break;
                }
            }

            if(found==false){
                cart.push(product);
            }

            localStorageService.add("local_cart", cart);

        };

        this.update_cart = function (){
            console.log(cart);
            localStorageService.add('local_cart',cart);
        }

        this.delete_from_cart = function(sel_cart_item){
            for ( var i=0; i<cart.length; i++){
                if (cart[i].parent_prod_detail.sku ==sel_cart_item.parent_prod_detail.sku){
                    cart.splice(i,1);
                }
            }
            localStorageService.add('local_cart',cart);
        }

        this.save_cart_to_db = function($http, $q){

           // var apiPath = 'http://localhost:8185' + '/store/cart/write_bulk_cart/';
            var apiPath = cat_service_url  + '/store/cart/write_bulk_cart/';
            var deferred = $q.defer();
            var cart_items=JSON.stringify(cart);

            $http({
                method: 'POST',
                url: apiPath,
                data: {email_id:localStorageService.get('user_info').email_id, cart_items:cart_items},
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).success(function (data) {
                deferred.resolve(data);
                //localStorageService.add('user_info', data[0]);
            }).error(function (data)
            {
                deferred.reject("An error occured while validating User");
            })
            return deferred.promise;


        }

        this.get_bulk_cart_by_email = function ($http, $q, email_id) {
            // var apiPath = 'http://localhost:8185' + "/store/bulk_cart?email_id=" + email_id + "&json=true";
            var apiPath = cat_service_url  + "/store/bulk_cart?email_id=" + email_id + "&json=true";

            var deferred = $q.defer();

            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })
            return deferred.promise;
        };




        var cart =[
            {
//            prod_id: 'ABCDFER1', quantity: 2, unit_price: 1400.5,  price:2801.00
            }

        ];



//    this.add_cart = function ($http, $q, dataobj) {
//
//        var apiPath = 'http://localhost:8181/checkout/cart/';
//        var deferred = $q.defer();
//        $http({
//            method: 'POST',
//            url: apiPath,
//            data: dataobj,
//            type: JSON
//        }).success(function (data) {
//            deferred.resolve(data);
//            localStorageService.add('cart_id', data);
//        }).error(function (data) {
//            deferred.reject("An error occured while validating User");
//        })
//        return deferred.promise;
//
//    };


        this.get_cart_id = function () {

            var cart_id = '';
            cart_id = localStorageService.get('cart_id');
            return cart_id;
        }



        /********************Added Code For Wish List***********************/

        this.load_wishlist_from_storage = function () {

            if ((wishlist.length == 1) || (wishlist.length == 0)) {
                if (localStorageService.get('local_wishlist') != null) {
                    wishlist = localStorageService.get('local_wishlist');
                }
            }
        }

        this.get_wishlist = function () {

            if ((wishlist.length == 1) || (wishlist.length == 0)) {
                if ((wishlist.length == 1) && (typeof wishlist[0].id === 'undefined')) {
                    //console.log("inside");
                    // for first time browser load assuming wishlist is stored in localstorage
                    this.load_wishlist_from_storage();
                }
            }
            //console.log(wishlist);
            if (wishlist != null) {
                if ((wishlist[0] != null) && (wishlist[0].id == null)) {
                    wishlist.splice(0, 1);
                }
            }
            return wishlist;
        };
        this.get_wishlist_amount = function () {

            var tmp = this.get_wishlist();
            var total_amount;
            var min_shipping = utilService.get_min_shipping();
            var shipping_charge = utilService.get_shipping_charge();
            var shipping = 0;
            var total_amount_billable;
            var total_amount_final_ord = 0;
            for (var i = 0; i < tmp.length; i++) {
                total_amount_final_ord = total_amount_final_ord + tmp[i].final_offer * tmp[i].quantity;
            }

            if (total_amount_final_ord < min_shipping) {
                shipping = shipping_charge;
            }
            else {
                shipping = 0;
            }
            total_amount_billable = total_amount_final_ord + shipping;
            return total_amount_billable;
        };
        this.get_wishlist_Size = function () {

            if ((wishlist.length == 1) || (wishlist.length == 0)) {
                this.load_wishlist_from_storage();
            }
            var size;
            size = 0;
            for (var i = 1; i < wishlist.length; i++) {
                size = size + wishlist[i].quantity;
            }
            return size;
        };
        this.get_wishlist_Item_Size = function () {

            var wishlist_size = 0;
            for ( var i = 0; i < wishlist.length; i++) {
                wishlist_size = wishlist_size + parseInt(wishlist[i].quantity);
            }
            return wishlist_size;
        };
        this.update_to_new_wishlist = function (new_wishlist) {

            localStorageService.add('local_wishlist', new_wishlist);

        }
        this.update_wishlist_item_size = function (wishlist_item_id, new_sku) {

            for ( var i = 0; i < wishlist.length; i++) {
                if (wishlist[i].wishlist_item_id == wishlist_item_id) {
                    wishlist[i].sku = new_sku;
                    break;
                }
            }
            localStorageService.add('local_wishlist', wishlist);
        }

        this.load_wishlist = function (wishlist_item) {

            wishlist.push({
                wishlist_item_id: wishlist_item.sku,
                id: wishlist_item._id,
                sku: wishlist_item.sku,
                brand: wishlist_item.brand,
                quantity: wishlist_item.quantity,
                mrp: wishlist_item.mrp,
                final_offer: wishlist_item.final_offer,
                name: wishlist_item.Name,
                image: wishlist_item.image_urls[0],
                discount: wishlist_item.discount,
                sizes: wishlist_item.sizes,
                express: wishlist_item.express
            });
            localStorageService.add('local_wishlist', wishlist);
        }


        this.addTowishlist = function (product, selected_child_sku, sizes) {

            // selected_child_sku is passed only when there is child product
            // otherwise we assume no child prod and we deal with parent prod only
            var found;
            found = false;
            for (var i = 0; i < wishlist.length; i++) {
                if ((selected_child_sku != null) && (selected_child_sku != "")) {       // only when there is child product
                    if (wishlist[i].sku == selected_child_sku.sku) {
                        wishlist[i].quantity = wishlist[i].quantity + 1;
                        found = true;
                    }
                }
                else {
                    if (wishlist[i].id == product.id) {
                        wishlist[i].quantity = wishlist[i].quantity + 1;
                        found = true;
                    }
                }
            }

            // ensure price part of the product structure is an object and not a string
            var product_price = null;
            if ((typeof product.price) != "object") {
                product_price = JSON.parse(product.price);
            }
            else {
                product_price = product.price;
            }

            // set express
            var express;
            if (product.Express == null) {
                express = false;
            }
            else if (product.Express == 0) {
                express = false;
            }
            else {
                express = true;
            }
            //alert (localStorageService.get("user_info").email_id)    ;
            if ((selected_child_sku != null) && (selected_child_sku != "")) {       // only when there is child product
                if (!found) {
                    wishlist.push({
                        //id: selected_child_sku.id,
                        //wishlist_item_id : selected_child_sku.sku,
                        cart_item_id: selected_child_sku.sku,
                        id: product.id,
                        //                    _id: wishlist_item._id,
                        sku: selected_child_sku.sku,
                        brand: product.brand,
                        quantity: 1,
                        mrp: product_price.mrp,
                        final_offer: product_price.final_offer,
                        name: product.Name,
                        image: product.image_urls[0],
                        discount: (((product_price.mrp - product_price.final_offer) / product_price.mrp) * 100).toFixed(0),
                        sizes: sizes,
                        selected_size: selected_child_sku.size,
                        express: express
                    });

                }
            }
            else {
                if (!found) {
                    wishlist.push({
                        cart_item_id: product.sku,
                        id: product.id,
                        //                    _id: wishlist_item._id,
                        sku: product.sku,
                        brand: product.brand,
                        quantity: 1,
                        mrp: product_price.mrp,
                        final_offer: product_price.final_offer,
                        name: product.Name,
                        image: product.image_urls[0],
                        discount: (((product_price.mrp - product_price.final_offer) / product_price.mrp) * 100).toFixed(0),
                        sizes: sizes,
                        selected_size: "ONESIZE",
                        express: express
                    });

                }
            }
            localStorageService.add('local_wishlist', wishlist);
            //this.save_wishlist_to_db();
            //alert (localStorageService.get('local_wishlist')) ;
        };

        this.update_wishlist = function () {
            console.log(wishlist);
            localStorageService.add('local_wishlist', wishlist);
        }

        this.delete_from_wishlist = function (id) {
            for (var i = 0; i < wishlist.length; i++) {
                if (wishlist[i].id == id) {
                    wishlist.splice(i, 1);
                }
            }
            localStorageService.add('local_wishlist', wishlist);
        }

        this.save_wishlist_to_db = function ($http, $q) {
            var server_wishlist = {};
            server_wishlist.email_id = localStorageService.get('user_info').email_id;
            server_wishlist.wishlist_items = wishlist;
            var apiPath = cat_service_url + '/store/wishlist/write_wishlist/';
            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: apiPath,
                data: server_wishlist,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
                //localStorageService.add('user_info', data[0]);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })
            return deferred.promise;

        }


        var wishlist = [
            {
                //            prod_id: 'ABCDFER1', quantity: 2, unit_price: 1400.5,  price:2801.00
            }
        ];
  });
