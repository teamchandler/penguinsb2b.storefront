'use strict';

angular.module('exidelife.web')
  .service('utilService', function utilService(localStorageService) {
    // AngularJS will instantiate a singleton by calling "new" on this function
        // now it's sourcing the config from local js ... if needed we can get that from other sources
        this.get_store_type = function () {
            return rule_list.store_type;
        };
        this.get_payment_gateway = function () {
            return payment_gateway;
        };

        // return point conversion ratio
        this.get_conversion_ratio = function () {
            return rule_list.conv_ratio;
        };
        // return customer discount
        this.get_customer_discount = function () {
            return rule_list.cust_discount;
        };
        // return customer discount
        this.get_cat_discount = function () {
            return rule_list.cat_discount;
        };
        // return customer discount
        this.get_sku_discount = function () {
            return rule_list.sku_discount;
        };

        // return brand discount
        this.get_brand_discount = function () {
            return rule_list.brand_discount;
        };


        // return min_shipping
        this.get_min_shipping = function () {
            return rule_list.min_shipping;
        };

        // return min_shipping
        this.get_shipping_charge = function () {
            return rule_list.shipping_charge;
        };

        // return newarrivals_threshhold
        this.get_newarrivals_threshhold = function () {
            return rule_list.newarrivals_threshhold;
        };


        // return premium_threshhold
        this.get_premium_threshhold = function () {
            return rule_list.premium_threshhold;
        };

        // return express_threshhold
        this.get_express_threshhold = function () {
            return rule_list.express_threshhold;
        };
        // return store
        this.get_store = function () {
            return store;
        };

        this.get_city_list = function () {
            return city_list;
        };
        // Local Ad Banners
        this.local_banners_exist = function () {
            if (JSON.stringify(adItems) === '{}'){
                return false;
            }
            else {
                return true;
            }
        };

        this.get_landing_page_scroller = function () {
            return adItems.landing_page_main_scroller
        };
        this.get_l1_banner = function () {
            return adItems.l1_banner
        };
        // return brand exclusion
        this.get_brand_exclusion= function () {
            if (brand_exclusion === 'undefined'){
                return [];
            }
            else {
                return brand_exclusion;
            }
        };

        this.get_cat_exclusion = function () {
            if (typeof cat_exclusion === 'undefined') {
                return [];
            }
            else {
                return cat_exclusion;
            }
        };


        this.get_express_shipping = function () {
            return express_shipping;
        };

        this.get_threshold_value = function () {
            return threshold_value;
        };



        this.get_target_management = function () {
            if (typeof target_management === 'undefined') {
                return 0;
            }
            else {
                return target_management;
            }
        };

        this.get_filter_display = function () {

            return filter_display;

        };

        this.change_state = function ($state, state_name, params){
            $state.go(state_name, params, "{location: true}") ;
        }

        // for next buttons
        var nextList = [];
        var selected_item = "";
        this.set_next_list = function(arr){
            localStorageService.add('next_list', arr);
            nextList = arr;
        }
        this.get_next_list = function(){
            if  (localStorageService.get('next_list') != null)
            {
                nextList  =localStorageService.get('next_list');
            }
            else {
                nextList  = [];
            }
            return nextList;
        }

  });
