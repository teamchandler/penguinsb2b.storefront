'use strict';

angular.module('exidelife.web')
  .service('MenuService', function MenuService() {
    // AngularJS will instantiate a singleton by calling "new" on this function


        this.get_all_menu = function ($http, $q, company){
            //var apiPath = cat_service_url +  '/store/menu?json=true';
            var apiPath = cat_service_url +  '/store/menu/' + company + '?json=true';

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                //data: data,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };
        this.get_level1_menu = function ($http, $q){
            var apiPath = cat_service_url +  '/store/level1menu?json=true';

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                //data: data,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };
        this.get_menu_expiry = function ($http, $q, last_download_date){
            var apiPath = cat_service_url +  '/store/menu/expired?last_download_date=' +last_download_date +'&json=true';
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };
        this.getAds = function (cat_id) {
            return  menuItems[0].ad_urls;
        };
  });
