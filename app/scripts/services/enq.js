/**
 * Created by sweta on 6/21/2014.
 */

'use strict';

angular.module('exidelife.web')

    .service('enqService', function (localStorageService, utilService) {



    this.insertenquireinfo = function ($http, $q, dataobj) {
        var apiPath = cat_service_url + '/user/registration/insert_product_enquire_info';
        var deferred = $q.defer();

        $http({
            method: 'POST',
            url: apiPath,
            data: dataobj,
            type: JSON
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (data) {
            deferred.reject("An error occured while validating User");
        })
        return deferred.promise;
    };

    this.get_productDetailsById = function ($http, $q, product_id) {
        var apiPath = cat_service_url + '/store/prod_details?prod_id=' + product_id + '&json=true';

        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: apiPath,
            //data: data,
            type: JSON
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (data) {
            deferred.reject("An error occured while validating User");
        })

        return deferred.promise;
    };


});
