/**
 * Created by DRIPL_1 on 02/13/2015.
 */


'use strict';

angular.module('exidelife.web')
    .service('Cat_allService', function Cat_allService(localStorageService, utilService) {
        // AngularJS will instantiate a singleton by calling "new" on this function


        this.get_cat_by_brand = function ($http, $q, brand_name) {
            //var apiPath = 'http://localhost:8181/store/prod_list_by_cat_short?cat_id=' + cat_id + '&json=true';
            //console.log(max) ;
            var apiPath = cat_service_url + '/category/admin/cat_id_by_brand/'+brand_name;
            //console.log(apiPath);
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
               // data:brand_name,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };

    });