/**
 * Created by DRIPL_1 on 08/06/2014.
 */

'use strict';

angular.module('exidelife.web')
    .service('my_enquiryService', function CartService(localStorageService, utilService) {
// AngularJS will instantiate a singleton by calling "new" on this function

        this.get_enquire_data = function ($http, $q, user_id) {
            var apiPath = cat_service_url + '/user/registration/get_product_enquire_info?email=' + user_id +'&json=true' ;
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })
            return deferred.promise;
        };

        this.get_general_enquire_data = function ($http, $q, user_id) {
            var apiPath = cat_service_url + '/user/registration/get_general_enquire_info?email=' + user_id +'&json=true' ;
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })
            return deferred.promise;
        };





    });
