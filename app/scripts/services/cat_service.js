'use strict';

angular.module('exidelife.web')
  .service('catService', function CatService() {
        this.get_cat = function ($http, $q, cat_id){
            var apiPath = cat_service_url +  '/store/cat_filters?cat_id=' + cat_id + '&json=true';

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                //data: data,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };
        this.get_cat_details = function ($http, $q, cat_id){
            var apiPath = cat_service_url +  '/store/cat_details?cat_id=' + cat_id + '&json=true';

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };
        this.get_breadcrumb = function ($http, $q, cat_id){
            var apiPath = cat_service_url +  '/store/breadcrumb?type=cat&value=' + cat_id + '&json=true';

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                //data: data,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };
        this.get_prod_by_cat = function ($http, $q, cat_id, min, max){
            //var apiPath = 'http://localhost:8181/store/prod_list_by_cat_short?cat_id=' + cat_id + '&json=true';
            //console.log(max) ;
            var apiPath = cat_service_url + '/store/prod_list_by_cat_short?cat_id=' + cat_id + '&min=' + min + '&max=' + max + '&json=true';
            //console.log(apiPath);
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                //data: data,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };


        this.get_prod_by_special_cat = function ($http, $q, menu_type, price_range, parent_cat_id, min, max){
            //var apiPath = 'http://localhost:8181/store/prod_list_by_cat_short?cat_id=' + cat_id + '&json=true';
            //console.log(max) ;
            var apiPath = cat_service_url + '/store/prod_list_by_special_cat_short?menu_type=' + menu_type + '&price_range=' + price_range + '&parent_cat_id=' + parent_cat_id  + '&min=' + min + '&max=' + max + '&json=true';
            //console.log(apiPath);
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                //data: data,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };


        this.get_prod_by_cat_sorted = function ($http, $q, cat_id, asc, desc){
            //var apiPath = 'http://localhost:8181/store/prod_list_by_cat_short?cat_id=' + cat_id + '&json=true';
            var apiPath = cat_service_url + '/store/prod_list_by_cat_short_sorted?cat_id=' + cat_id + '&asc='+asc+'&desc='+desc+'&json=true';
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                //data: data,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };
        this.get_prod_by_cat_filter = function ($http, $q, cat_id, unsorted_filter){
            var qs = "{cat_id:{cat_id:'" + cat_id + "'}";
            var tmp_new_name=""   ;
            var tmp_old_name ="";
            var filter=  _(unsorted_filter).sortBy(function(obj) { return obj.name })
            //console.log(filter);
            //filter.sort(mySorting);
            //alert (filter[0].name + "-" + filter[1].name + "-" + filter[2].name + "-" + filter[3].name);
            for (var i=0; i<filter.length; i++){
                tmp_new_name = filter[i].name;
                if (tmp_new_name != tmp_old_name) {
                    // new filter name
                    if (tmp_old_name == ""){
                        // first time
                        qs = qs+",'feature."+ filter[i].name + "':{$in :['" +filter[i].value + "'";
                    }
                    else{
                        qs = qs+"]},'feature."+ filter[i].name + "':{$in :['" +filter[i].value + "'";
                    }
                }
                else{
                    if (tmp_old_name == ""){
                        // first time
                        qs = qs+"','feature."+ filter[i].name + "':$in :{['" +filter[i].value + "'";
                    }
                    else{
                        qs = qs+",'" + filter[i].value + "'";
                    }
                }
                // qs = qs+",'feature."+ filter[i].name + "':'" +filter[i].value + "'";
                tmp_old_name =  filter[i].name;
                if (i==filter.length-1){ // reached to end
                    qs=qs+"]";
                }
            }
            qs = qs + "}}";

            console.log(qs);
            //qs="{cat_id: {'id': '1.1.1'}}";
            //qs="{cat_id: {'id': '1.1.1'},'feature.Color':{$in :['Blue']}}";
            //console.log(qs);
            var apiPath = cat_service_url + '/store/prod_list_by_qs?json=true';

            var data = qs;
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data: data,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };





    });
