
'use strict';

angular.module('exidelife.web')
    .service('BrandService', function CartService(localStorageService, utilService) {
        // AngularJS will instantiate a singleton by calling "new" on this function

        this.get_brand_list = function ($http, $q , flag) {
            //var apiPath = 'http://localhost:8181/store/prod_list_by_cat_short?cat_id=' + cat_id + '&json=true';
            //console.log(max) ;
            var apiPath = cat_service_url + '/category/brand_list/'+ flag ;
            //console.log(apiPath);
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                //data: data,s
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };



        this.get_product_by_brand = function ($http, $q, brand_name) {
            //var apiPath = 'http://localhost:8181/store/prod_list_by_cat_short?cat_id=' + cat_id + '&json=true';
            //console.log(max) ;
            var apiPath = cat_service_url + '/store/get_prod_by_brand?brand_name=' + brand_name + '&json=true';
            //console.log(apiPath);
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                //data: data,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };

    });
