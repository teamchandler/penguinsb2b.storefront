/**
 * Created by Hassan Ahamed on 2/11/2015.
 */
'use strict';

angular.module('exidelife.web')
 .service('activationService', function () {

    this.activation = function ($http, $q, dataobj) {
        var apiPath = cat_service_url +   '/user/registration/activate_registration';
        var deferred = $q.defer();

        $http({
            method: 'POST',
            url: apiPath,
            data: dataobj,
            type: JSON
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (data)
        {
            deferred.reject("An error occured while validating User");
        })
        return deferred.promise;

    };
});





