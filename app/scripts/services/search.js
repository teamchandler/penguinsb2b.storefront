/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 21/7/13
 * Time: 8:39 AM
 * To change this template use File | Settings | File Templates.
 */

'use strict';


angular.module('exidelife.web')
    .service('searchService', function () {
    this.get_filters = function ($http, $q, srch){
        var apiPath = cat_service_url +  '/store/search_filters?srch=' + srch+ '&json=true';

        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: apiPath,
            type: JSON
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (data) {
            deferred.reject("Search Query Error");
        })

        return deferred.promise;
    };

    this.get_search_prod_list = function ($http, $q, srch, context, min, max){
        //var apiPath = 'http://localhost:8181/store/prod_list_by_cat_short?cat_id=' + cat_id + '&json=true';
        var apiPath = cat_service_url + '/store/search?srch=' + srch+ '&context='+ context + '&min=' + min + '&max=' + max + '&json=true';
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: apiPath,
            //data: data,
            type: JSON
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (data) {
            deferred.reject("An error occured while validating User");
        })

        return deferred.promise;
    };


});
