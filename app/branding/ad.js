/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 20/7/13
 * Time: 9:29 AM
 * To change this template use File | Settings | File Templates.
 */
var adItems = {
    "landing_page_main_scroller" : [{
        "href" : "#",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/newyear/homeny.jpg"
    },{
        "href" : "#",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/bulkhome/pumabanner.jpg"
         }
// ,{
//        "href" : "#/cat/1.1.1",
//        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/puma/MENS.jpg"
//    }
    ],
    "landing_page_static_html" : "https://s3-ap-southeast-1.amazonaws.com/annectos/ads/men_static_ad.html",
    "offer_items" : [{
        "id" : "001",
        "offer_text" : "Only till 24th July midnight | Additional 10% off on Nike Shoes only for Intel employees",
        "href" : "#/cat/1.0/"
    }, {
        "id" : "002",
        "offer_text" : "Additional 5% off on Women Hand Bags only for Intel employees",
        "href" : "#/cat/2.0/"
    }],
    "l1_banner" : [{
        "id" : "men",
        "href" : "#/cat/2.4.1",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/spanbanner/spantimex.jpg"
    }, {
        "id" : "men",
        "href" : "#/cat/2.4.3",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/spanbanner/spanreebokbags.jpg"
    }, {
        "id" : "kids",
        "href" : "#/cat/4.2.0",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/kids/Kids_disney_bags.jpg"
    }, {
        "id" : "kids",
        "href" : "#/cat/4.2.0",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/spanbanner/spancrocskids.jpg"
    }, {
        "id" : "mt",
        "href" : "#/cat/7.1.0",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/spanbanner/spanmobiletablets.jpg"
    }, {
        "id" : "mt",
        "href" : "#/cat/7.1.1",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/mobiletablets/Mobile_Samsung.jpg"
    }, {
        "id" : "hk",
        "href" : "#/cat/6.4.0",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/spanbanner/spanbajaj.jpg"
    }, {
        "id" : "hk",
        "href" : "#/cat/6.3.2",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/spanbanner/spaniveo.jpg"
    }, {
        "id" : "etc",
        "href" : "#/cat/5.2.3",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/spanbanner/spanenzatech.jpg"
    }, {
        "id" : "etc",
        "href" : "#/cat/5.2.1",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/spanbanner/spanucb.jpg"
    },  {
        "id" : "women",
        "href" : "#/cat/3.2.6",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/spanbanner/spanleggings.jpg"
    }, {
        "id" : "women",
        "href" : "#/cat/3.2.0",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/spanbanner/spansaree.jpg"
    }]
};