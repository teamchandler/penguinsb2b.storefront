/**
 * Created with JetBrains WebStorm.
 * User: Hassan Ahamed
 * Date: 11/02/15
 * Time: 16:41 AM
 * To change this template use File | Settings | File Templates.
 */


//var cat_service_url= "http://localhost:8185";
  //var root_url= "http://qa.exidelife.annectos.net";
var cat_service_url= "http://app.annectos.net/ecomm.penguinsb2b.in.api";
var s3_location_url = "https://s3-ap-southeast-1.amazonaws.com/annectos/";
var order_service_url= "http://app.qa.annectos.net/ecomm.api";
var s3_brand_logo_url ="http://cdn-new-annectos.s3.amazonaws.com/images/brandlogo/n/";
var store = "penguinsb2b";
var store_signature = "penguinsb2b";
var store_email_signature = "*.*";
var payment_gateway = 1;
var target_management = 1;
var brand_exclusion = ['ANNECTOS','SHERIE','HOTNHOT'];
var express_shipping = 1;
var cat_exclusion = ['Mobiles & Tablets', 'Home & Kitchen'];
var filter_display = "R";
var threshold_value= 100;
var city_list = [
    {  text: "Ahmedabad" },
    {  text: "Agra" },
    {  text: "Bangalore" },
    {  text: "Bhopal" },
    {  text: "Chandigarh" },
    {  text: "Chennai" },
    {  text: "Cochin" },
    {  text: "Dehradun" },
    {  text: "Delhi" },
    {  text: "Ghaziabad" },
    {  text: "Hyderabad" },
    {  text: "Indore" },
    {  text: "Jaipur" },
    {  text: "Kanpur" },
    {  text: "Kerala" },
    {  text: "Kolkata" },
    {  text: "Lucknow" },
    {  text: "Ludhiana" },
    {  text: "Mumbai" },
    {  text: "Nagpur" },
    {  text: "Patna" },
    {  text: "Pune" },
    {  text: "Surat" },
    {  text: "Thane" },
    {  text: "Vadodara" },
    {  text: "Visakhapatnam" }
];


