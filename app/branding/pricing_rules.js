/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 23/7/13
 * Time: 5:45 AM
 * To change this template use File | Settings | File Templates.
 */
var rule_list = {

        "store_type": "R", // R=Only Retail, P = Only Point, RP = Retail and Point (default)
        "conv_ratio": 2, // 1.25 point = Rs 1 - If price = 1000 then equivalent point = 1250
        "min_shipping": 500,
        "shipping_charge":0,
        "premium_threshhold":5000,
        "rule_id": "1010",
        "cust_discount":"0",
        "cat_discount":
            [
                {"id": "1.1.1", "extra_off":".1"},
//                {"id": "2.1", "extra_off":".12"},
                {"id": "1.2", "extra_off":".08"}
            ],
        "sku_discount":
            [
                {"id": "leggings3wbgcombo2", "extra_off":"-.04"},
                {"id": "7890", "extra_off":".1"}
            ],
        "brand_discount":
            [
                { "brand": "bajaj", "extra_off": ".9" }
               
            ]
}
;
